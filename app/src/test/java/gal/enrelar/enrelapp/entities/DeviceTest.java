package gal.enrelar.enrelapp.entities;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeviceTest {
    private Device device;
    @Before
    public void createDevice (){
        String formattedString = "deviceId|/|lastValue|/|deviceAlias";
        device = new Device(formattedString);
    }
    @Test
    public void constructorTest (){
        assertEquals("deviceId|/|lastValue|/|deviceAlias", device.toString());
    }
}