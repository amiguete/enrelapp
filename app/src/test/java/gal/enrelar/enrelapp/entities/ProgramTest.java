package gal.enrelar.enrelapp.entities;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProgramTest {
    @Test
    public void createProgramNormal (){
        String formattedString = "proba1|/|00:00|/|23:59|/|0 1 2 3 4 5 6|/|01-01|/|12-31" +
                "|@|rele-DC:4F:22:FA:68:07|/|1|@|temperatura-BC:DD:C2:BA:FD:7A|/|menor|/|20";
        Program program = new Program(formattedString);
        assertEquals(formattedString, program.toString());
    }
    @Test
    public void createProgramNoCondition (){
        String formattedString = "proba1|/|00:00|/|23:59|/|0 1 2 3 4 5 6|/|01-01|/|12-31" +
                "|@|rele-DC:4F:22:FA:68:07|/|1|@|";
        Program program = new Program(formattedString);
        assertEquals(formattedString, program.toString());
    }
    @Test
    public void createProgramMultipleAction (){
        String formattedString = "proba1|/|00:00|/|23:59|/|0 1 2 3 4 5 6|/|01-01|/|12-31" +
                "|@|rele-DC:4F:22:FA:68:07|/|1|&|rele-DC:4F:22:FA:68:87|/|0" +
                "|@|temperatura-BC:DD:C2:BA:FD:7A|/|menor|/|20";
        Program program = new Program(formattedString);
        assertEquals(formattedString, program.toString());
    }
    @Test
    public void createProgramMultipleCondition (){
        String formattedString = "proba1|/|00:00|/|23:59|/|0 1 2 3 4 5 6|/|01-01|/|12-31" +
                "|@|rele-DC:4F:22:FA:68:07|/|1" +
                "|@|temperatura-BC:DD:C2:BA:FD:7A|/|menor|/|20|&|humidade-BC:DD:C2:BA:FD:7A|/|menor|/|50";
        Program program = new Program(formattedString);
        assertEquals(formattedString, program.toString());
    }
    @Test
    public void createProgramMultipleAll (){
        String formattedString = "proba1|/|00:00|/|23:59|/|0 1 2 3 4 5 6|/|01-01|/|12-31" +
                "|@|rele-DC:4F:22:FA:68:07|/|1|&|rele-DC:4F:22:FA:68:87|/|0" +
                "|@|temperatura-BC:DD:C2:BA:FD:7A|/|menor|/|20|&|humidade-BC:DD:C2:BA:FD:7A|/|menor|/|50";
        Program program = new Program(formattedString);
        assertEquals(formattedString, program.toString());
    }
}
