package gal.enrelar.enrelapp.entities;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConditionTest {
    private Condition condition;
    @Before
    public void createCondition (){
        String formattedString = "deviceId|/|comparator|/|action";
        condition = new Condition(formattedString);
    }
    @Test
    public void constructorTest (){
        assertEquals("deviceId|/|comparator|/|action", condition.toString());
    }
}