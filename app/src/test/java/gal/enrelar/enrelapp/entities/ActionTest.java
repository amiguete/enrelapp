package gal.enrelar.enrelapp.entities;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ActionTest {
    private Action action;
    @Before
    public void createAction (){
        String formattedString = "deviceId|/|action";
        action = new Action(formattedString);
    }
    @Test
    public void constructorTest (){
        assertEquals("deviceId|/|action", action.toString());
    }
}