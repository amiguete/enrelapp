package gal.enrelar.enrelapp;

import android.content.Context;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

public class Util {
    public static String fromTimeToString(Time time){
        //recibe un Time co formato HH:mm:ss e devolve un String con formato HH:mm
        return time.toString().substring(0,5);
    }

    public static String fromDateToString(Date date, boolean invert){
        //recibe un Date co formato yyyy-mm-dd e devolve un String con formato dd-mm
        String[] dateSplit = date.toString().split("-");
        if (invert){
            return  dateSplit[2] + "-" + dateSplit[1];
        } else{
            return dateSplit[1] + "-" + dateSplit[2];
        }

    }

    public static Time fromStringToTime(String time){
        //recibe un String con formato HH:m e devolve un Time
        return Time.valueOf(time + ":00");
    }

    public static Date fromStringToDate(String date, boolean invert){
        //recibe un String con formato dd-mm e devolve un Date
        Calendar cal = Calendar.getInstance();
        String[] dateSplit = date.split("-");
        String fullDate = cal.get(Calendar.YEAR) + "-";
        if (invert){
            fullDate += dateSplit[1] + "-";
            fullDate += dateSplit[0];
        }else{
            fullDate += dateSplit[0] + "-";
            fullDate += dateSplit[1];
        }
        return Date.valueOf(fullDate);
    }

    public static String fromNumberToDayOfWeek(String numbers, Context context){
        //cambia de 0 1 2 3 4 5 6 a do lu ma me xo ve sa
        numbers = numbers.replace("0", context.getString(R.string.sundayAbv));
        numbers = numbers.replace("1", context.getString(R.string.mondayAbv));
        numbers = numbers.replace("2", context.getString(R.string.tuesdayAbv));
        numbers = numbers.replace("3", context.getString(R.string.wednesdayAbv));
        numbers = numbers.replace("4", context.getString(R.string.thursdayAbv));
        numbers = numbers.replace("5", context.getString(R.string.fridayAbv));
        numbers = numbers.replace("6", context.getString(R.string.saturdayAbv));
        return numbers;
    }

    public static String fromDayOfWeekToNumber(String dayOfWeek, Context context){
        //cambia de do lu ma me xo ve sa a 0 1 2 3 4 5 6
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.sundayAbv), "0");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.mondayAbv),"1");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.tuesdayAbv),"2");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.wednesdayAbv),"3");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.thursdayAbv),"4");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.fridayAbv),"5");
        dayOfWeek = dayOfWeek.replace(context.getString(R.string.saturdayAbv),"6");
        return dayOfWeek;
    }

    public static InputStream fromStringToInputStream(String string){
        return new ByteArrayInputStream(string.getBytes());
    }
}
