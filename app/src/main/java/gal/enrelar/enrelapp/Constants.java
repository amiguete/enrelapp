package gal.enrelar.enrelapp;

public class Constants {
    //separadores usados polo coordinador e polos dispositivos no paso de mensaxes

    //separador de atributos de programa, acción, condición, e dispositivo
    public static final String ATTRIBUTES_SEPARATOR = "|/|";
    public static final String ATTRIBUTES_SPLITTER = "\\|/\\|";
    //separador para programas
    public static final String PROGRAMS_SPLITTER = "\\|¬\\|";
    // separa o conxunto de atributos do programa das accións e das condicións
    public static final String ELEMENTS_SEPARATOR = "|@|";
    public static final String ELEMENTS_SPLITTER = "\\|@\\|";
    //separador de multiples accións e condicións
    public static final String AND_SEPARATOR = "|&|";
    public static final String AND_SPLITTER = "\\|&\\|";

    //número segundo o tipo de conexión
    public static final int GENERATED_WIFI_PORT = 8883;
    public static final int LOCAL_PORT = 8884;
    public static final int INTERNET_PORT = 8885;
}
