package gal.enrelar.enrelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.adapters.TabPagerAdapter;
import gal.enrelar.enrelapp.managers.DeviceManager;
import gal.enrelar.enrelapp.managers.MqttManager;
import gal.enrelar.enrelapp.managers.ServerConnectionManager;

public class MainActivity extends AppCompatActivity {
    private TabPagerAdapter adapter;
    private int currentTab;
    private ServerConnectionManager serverConnectionManager;
    private MqttManager mqttManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        serverConnectionManager = ServerConnectionManager.getInstance(getApplicationContext());
        //tomar a barra de ferramentas da vista
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //tomar a barra de pestanas da vista
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        // Definir o texto de cada pestana.
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label1));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label2));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label3));
        // facer que as pestanas ocupen toda a barra de pestanas.
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        // Usar TabPagerAdapter para xestionar as vistas dos fragmentos en pestanas.
        // cada páxina está representada polo seu propio fragmento.
        final ViewPager viewPager = findViewById(R.id.pager);
        adapter = new TabPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        mqttManager = MqttManager.getInstance(getApplicationContext(), adapter);
        viewPager.setAdapter(adapter);
        // Definir comportamento dos clicks no paxinador.
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new
                                                   TabLayout.OnTabSelectedListener() {
                                                       @Override
                                                       public void onTabSelected(TabLayout.Tab tab) {
                                                           //cambia a variable currentTab para que o
                                                           //resto de métodos poidan modificar o
                                                           //comportamento en función da pestana
                                                           currentTab = tab.getPosition();
                                                           viewPager.setCurrentItem(currentTab);
                                                           //recrea o menú para actualizar os iconas segundo a pestana
                                                           invalidateOptionsMenu();
                                                       }

                                                       @Override
                                                       public void onTabUnselected(TabLayout.Tab tab) {
                                                       }

                                                       @Override
                                                       public void onTabReselected(TabLayout.Tab tab) {
                                                       }
                                                   });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        /*
        *este método chámase ó iniciar a aplicación
        * e cada vez que se chama a invalidateOptionsMenu
        * dende o listener de tabLayout
        */
        //(re)infla o menú
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //se a pestana seleccionada é a de conexións
        if(currentTab==0){
            //agocha o icona de refrescar
            MenuItem item = menu.findItem(R.id.refresh);
            item.setVisible(false);
        }
        //se a pestana seleccionada é a de dispositivos
        else if(currentTab==1) {
            //agocha o icona de engadir
            MenuItem item = menu.findItem(R.id.add);
            item.setVisible(false);
        }
        return true;
    }

    public void refreshClick(MenuItem menuItem){
        if (menuItem.getItemId()!=R.id.refresh){
            return;
        }
        //se a pestana seleccionada é a de dispositivos
        String topic = "";
        if(currentTab==1){
            topic = "status";
            DeviceManager deviceManager = DeviceManager.getInstance();
            deviceManager.clearDeviceList();
        }
        //se a pestana seleccionada é a de programas
        else if(currentTab==2) {
            topic = "program/getList";
        }
        if (!topic.isEmpty()) {
            if (mqttManager != null) {//se non hai conexión activa é nulo
                mqttManager.publishMessage(topic, "", this);
            }
        }
    }

    public void addClick(MenuItem menuItem){
        if (menuItem.getItemId()!=R.id.add){
            return;
        }
        //se a pestana seleccionada é a de conexións
        if(currentTab==0){
            Intent intent = new Intent(this, ManageServerConnectionActivity.class);
            intent.putExtra("action","add");
            startActivity(intent);
        }
        //se a pestana seleccionada é a de programas
        else if(currentTab==2) {
            Intent intent = new Intent(this, ManageProgramActivity.class);
            intent.putExtra("action","add");
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        //cando volve de outra actividade
        //recarga as pestanas
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onPause() {
        serverConnectionManager.saveChanges();
        super.onPause();
    }
}
