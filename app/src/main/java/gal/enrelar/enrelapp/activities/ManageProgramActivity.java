package gal.enrelar.enrelapp.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.Util;
import gal.enrelar.enrelapp.adapters.ActionArrayAdapter;
import gal.enrelar.enrelapp.adapters.ConditionArrayAdapter;
import gal.enrelar.enrelapp.entities.Action;
import gal.enrelar.enrelapp.entities.Condition;
import gal.enrelar.enrelapp.entities.Program;
import gal.enrelar.enrelapp.fragments.EditActionFragment;
import gal.enrelar.enrelapp.fragments.EditConditionFragment;
import gal.enrelar.enrelapp.managers.MqttManager;
import gal.enrelar.enrelapp.managers.ProgramManager;


public class ManageProgramActivity extends AppCompatActivity {
    //engadir, editar, eliminar un programa
    private EditText nameText;
    private EditText timeStartText;
    private EditText timeEndText;
    private EditText dateStartText;
    private EditText dateEndText;
    private TextView dayOfWeekText;
    private final ProgramManager programManager = ProgramManager.getInstance();
    private Program program;
    private ArrayList<Condition> conditionsList;
    private ArrayList<Action> actionsList;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manageprogram);
        //barra superior
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //engadir frecha de volver atrás
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                onBackPressed();
            }
        });

        // Iniciar variables.
        nameText = findViewById(R.id.name);
        timeStartText = findViewById(R.id.timeStart);
        timeEndText = findViewById(R.id.timeEnd);
        dateStartText = findViewById(R.id.dateStart);
        dateEndText = findViewById(R.id.dateEnd);
        dayOfWeekText = findViewById(R.id.dow);
        dayOfWeekText.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        ImageView timeStartIcon = findViewById(R.id.timeStartIcon);
        ImageView timeEndIcon = findViewById(R.id.timeEndIcon);
        ImageView dateStartIcon = findViewById(R.id.dateStartIcon);
        ImageView dateEndIcon = findViewById(R.id.dateEndIcon);
        ImageView addConditionIcon = findViewById(R.id.addConditionIcon);
        ImageView addActionIcon = findViewById(R.id.addActionIcon);
        //poñer o foco fora do nameText para que non despregue o teclado
        findViewById(R.id.nameTitle).requestFocus();
        //coller o parámetro de entrada para saber se é editar ou crear novo programa
        Intent intent = getIntent();
        String action = intent.getStringExtra("action");
        if (action == null) {//evitar warning
            action = "no_action";
        }
        if (action.equals("edit")) {//se foi chamada dende a lista de programas
            //toma os valores do programa a editar
            //posición do programa na lista de programas, pasada como parámetro
            int index = intent.getIntExtra("id", 0);
            program = programManager.getProgram(index);
            //cubre os campos editables cos valores do programa
            nameText.setText(program.getName());
            timeStartText.setText(Util.fromTimeToString(program.getTimeStart()));
            timeEndText.setText(Util.fromTimeToString(program.getTimeEnd()));
            dateStartText.setText(Util.fromDateToString(program.getDateStart(), true));
            dateEndText.setText(Util.fromDateToString(program.getDateEnd(), true));
            dayOfWeekText.setText(Util.fromNumberToDayOfWeek(program.getDayOfWeek(), this));
        }
        if (savedInstanceState == null) {//a primeira vez que abre a actividade
            conditionsList = new ArrayList<>();
            actionsList = new ArrayList<>();
            if (action.equals("edit")) {//e está editando un programa
                conditionsList.addAll(program.getConditionList());
                actionsList.addAll(program.getActionList());
            }
        } else {//se está recreando a actividade xa hai unha lista gardada
            conditionsList = (ArrayList<Condition>) savedInstanceState.getSerializable("conditionsList");
            actionsList = (ArrayList<Action>) savedInstanceState.getSerializable("actionsList");
        }
        recreateListView();
        final Calendar calendar = Calendar.getInstance();

        //Xestión de pickers (hora inicio, hora fin, data inicio, data fin,
        //                      día da semana, engadir condición)
        //picker de hora de inicio
        //o seguinte obxecto encárgase de tomar os datos cando se pecha o picker
        final TimePickerDialog.OnTimeSetListener timeStartListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = "";
                if (hourOfDay < 10) {
                    time += "0";
                }
                time += String.valueOf(hourOfDay);
                time += ":";
                if (minute < 10) {
                    time += "0";
                }
                time += String.valueOf(minute);
                timeStartText.setText(time);
            }
        };
        //cando se preme na icona do calendario ábrese un diálogo con DatePicker que lle pasará os datos ó obxecto anterior
        timeStartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(ManageProgramActivity.this, timeStartListener, calendar
                        .get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show();
            }
        });

        //picker de hora fin
        //o seguinte obxecto encárgase de tomar os datos cando se pecha o picker
        final TimePickerDialog.OnTimeSetListener timeEndListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = "";
                if (hourOfDay < 10) {
                    time += "0";
                }
                time += String.valueOf(hourOfDay);
                time += ":";
                if (minute < 10) {
                    time += "0";
                }
                time += String.valueOf(minute);
                timeEndText.setText(time);
            }
        };
        //cando se preme na icona do calendario ábrese un diálogo con DatePicker que lle pasará os datos ó obxecto anterior
        timeEndIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(ManageProgramActivity.this, timeEndListener, calendar
                        .get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show();
            }
        });

        //picker de data inicio
        //o seguinte obxecto encárgase de tomar os datos cando se pecha o picker
        final DatePickerDialog.OnDateSetListener dateStartListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = "";
                if (dayOfMonth < 10) {
                    date += "0";
                }
                date += String.valueOf(dayOfMonth);
                date += "-";
                monthOfYear ++;
                if (monthOfYear < 10) {
                    date += "0";
                }
                date += String.valueOf(monthOfYear);
                dateStartText.setText(date);
            }
        };
        //cando se preme na icona do calendario ábrese un diálogo con DatePicker que lle pasará os datos ó obxecto anterior
        dateStartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ManageProgramActivity.this, dateStartListener, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //picker de data fin
        //o seguinte obxecto encárgase de tomar os datos cando se pecha o picker
        final DatePickerDialog.OnDateSetListener dateEndListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = "";
                if (dayOfMonth < 10) {
                    date += "0";
                }
                date += String.valueOf(dayOfMonth);
                date += "-";
                monthOfYear++;//devolve os meses de 0 a 11
                if (monthOfYear < 10) {
                    date += "0";
                }
                date += String.valueOf(monthOfYear);
                dateEndText.setText(date);
            }
        };
        //cando se preme na icona do calendario ábrese un diálogo con DatePicker que lle pasará os datos ó obxecto anterior
        dateEndIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ManageProgramActivity.this, dateEndListener, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //picker de día semana
        final CharSequence[] allDays = new CharSequence[7];//array de Strings cos días da semana
        allDays[0] = getString(R.string.monday);
        allDays[1] = getString(R.string.tuesday);
        allDays[2] = getString(R.string.wednesday);
        allDays[3] = getString(R.string.thursday);
        allDays[4] = getString(R.string.friday);
        allDays[5] = getString(R.string.saturday);
        allDays[6] = getString(R.string.sunday);
        final boolean[] selectedDays = new boolean[7];//días seleccionados
        //o seguinte obxecto encárgase de tomar os datos e gardalos en selectedDays cando se pecha o picker
        final DialogInterface.OnMultiChoiceClickListener dayOfWeekListener =
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                                        boolean isChecked) {
                        selectedDays[which] = isChecked;
                    }
                };
        //cando se preme en días da semana ábrese un diálogo que permite seleccionar os días da semana
        //cando se preme en OK crea e pasa a lista dos días á vista de dayOfWeek usando o selectedDays
        dayOfWeekText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cargar os días xa seleccionados no array de boolean
                String previousSelectedDays =
                        Util.fromDayOfWeekToNumber(dayOfWeekText.getText().toString(),
                                ManageProgramActivity.this);
                for (int i = 0; i < 6; i++) {//de luns a sábado
                    CharSequence dayNumber = String.valueOf(i + 1);
                    selectedDays[i]= previousSelectedDays.contains(dayNumber);
                }
                selectedDays[6]= previousSelectedDays.contains("0");
                //xerar un constructor de diálogo
                new AlertDialog.Builder(ManageProgramActivity.this)
                        .setTitle(getString(R.string.pick))
                        .setMultiChoiceItems(allDays, selectedDays, dayOfWeekListener)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                StringBuilder selectedDaysString = new StringBuilder();
                                for (int i = 0; i < 6; i++) {//do luns ó sábado
                                    if (selectedDays[i]) {
                                        //i + 1 porque o luns é o día 1 e o sábado o 6
                                        selectedDaysString.append(i + 1).append(" ");
                                    }
                                }
                                //engadir ó final (se procede) o domingo
                                if (selectedDays[6]) {
                                    selectedDaysString.append("0");
                                }
                                //eliminar o último espazo (se o hai)
                                if (selectedDaysString.toString().endsWith(" ")) {
                                    selectedDaysString = new StringBuilder(selectedDaysString.substring
                                            (0, selectedDaysString.lastIndexOf(" ")));
                                }
                                //converter de número a día da semana
                                selectedDaysString = new StringBuilder(Util.fromNumberToDayOfWeek
                                        (selectedDaysString.toString(), ManageProgramActivity.this));
                                dayOfWeekText.setText(selectedDaysString.toString());
                            }
                        })
                        .setNegativeButton(R.string.cancel, null).show();
            }
        });
        //Icona de engadir condición
        addConditionIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fragmentManagerTag = "add_condition";
                EditConditionFragment editConditionDialog;
                editConditionDialog =new EditConditionFragment(conditionsList, -1);
                editConditionDialog.show(getSupportFragmentManager(),fragmentManagerTag);
            }
        });
        //Icona de engadir acción
        addActionIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fragmentManagerTag = "add_action";
                EditActionFragment editActionDialog;
                editActionDialog =new EditActionFragment(actionsList, -1);
                editActionDialog.show(getSupportFragmentManager(),fragmentManagerTag);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflar o menu cas iconas de eliminar e gardar
        getMenuInflater().inflate(R.menu.menu_managers, menu);
        if (program == null) {//foi chamada dende engadir
            //non é preciso botón de eliminar
            MenuItem item = menu.findItem(R.id.remove);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // gardar as listas
        savedInstanceState.putSerializable("conditionsList", conditionsList);
        savedInstanceState.putSerializable("actionsList", actionsList);
        // chamar a super clase para que garde o resto de cousas
        super.onSaveInstanceState(savedInstanceState);
    }

    public void saveClick(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.save) {
            return;
        }
        // foi pulsado o botón de gardar
        // campos precisos para crear ou editar o programa
        String name;
        String timeStartString;
        Time timeStart;
        String timeEndString;
        Time timeEnd;
        String dateStartString;
        Date dateStart;
        String dateEndString;
        Date dateEnd;
        String dayOfWeek;

        //tomar o nome dende a vista
        name = nameText.getText().toString();
        //comprobar que o nome estea ben cuberto
        if (name.matches("[ -]]*") || name.isEmpty()) {
            nameText.setError(getString(R.string.emptyFieldAd));
            return;
        }
        //tomar a hora de inicio dende a vista
        timeStartString = timeStartText.getText().toString();
        if (!timeStartString.matches("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$")) {
            timeStartText.setError(getString(R.string.badHourAd));
            return;
        }
        timeStart = Util.fromStringToTime(timeStartString);

        //tomar a hora de fin dende a vista
        timeEndString = timeEndText.getText().toString();
        if (!timeEndString.matches("([0-1]?[0-9]|2[0-3]):[0-5][0-9]$")) {
            timeEndText.setError(getString(R.string.badHourAd));
            return;
        }
        timeEnd = Util.fromStringToTime(timeEndString);
        //comprobar que as horas están en orde
        if (timeEnd.before(timeStart)) {
            timeEndText.setError(getString(R.string.incoherentTimeAd));
            return;
        }
        //tomar a data de inicio dende a vista
        dateStartString = dateStartText.getText().toString();
        if (!dateStartString.matches("^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])$")) {
            dateStartText.setError(getString(R.string.badDateAd));
            return;
        }
        dateStart = Util.fromStringToDate(dateStartString, true);
        //tomar a data de fin dende a vista
        dateEndString = dateEndText.getText().toString();
        if (!dateEndString.matches("^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])$")) {
            dateEndText.setError(getString(R.string.badDateAd));
            return;
        }
        dateEnd = Util.fromStringToDate(dateEndString, true);
        //comprobar que as datas están en orde
        if (dateEnd.before(dateStart)) {
            dateEndText.setError(getString(R.string.incoherentDateAd));
            return;
        }
        //tomar os días da semana dende a vista
        dayOfWeek = Util.fromDayOfWeekToNumber(dayOfWeekText.getText().toString(), this);

        //construír un obxecto Program
        String topic;
        if (program == null) {//se foi chamada dende engadir
            // hai que crear nova instancia
            program = new Program();
            topic = "program/add";
        } else {
            topic = "program/edit/" + program.getProgramId();
        }
        program.setName(name);
        program.setTimeStart(timeStart);
        program.setTimeEnd(timeEnd);
        program.setDateStart(dateStart);
        program.setDateEnd(dateEnd);
        program.setDayOfWeek(dayOfWeek);
        program.setConditionList(conditionsList);
        program.setActionList(actionsList);
        String message = program.toString();
        MqttManager mqttManager = MqttManager.getInstance();
        mqttManager.publishMessage(topic, message, this);
        finish();
    }

    public void removeClick(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.remove) {
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.ad))
                .setMessage(getString(R.string.confirmRemove))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String topic = "program/del";
                        String message = String.valueOf(program.getProgramId());
                        MqttManager mqttManager = MqttManager.getInstance();
                        mqttManager.publishMessage(topic, message, ManageProgramActivity.this);
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, null).show();


    }

    public void recreateListView() {
        final ViewGroup tableGroup = findViewById(R.id.table);
        //adaptador para amosar unha lista de condicións
        final ConditionArrayAdapter conditionArrayAdapter = new ConditionArrayAdapter(this, R.layout.list_view_condition, conditionsList);
        //adaptador para amosar unha lista de accións
        final ActionArrayAdapter actionArrayAdapter = new ActionArrayAdapter(this, R.layout.list_view_action, actionsList);
        //contedores das listas, non se usa listView por estar dentro dun scrollView
        final LinearLayout conditionListView = findViewById(R.id.conditionListView);
        final LinearLayout actionListView = findViewById(R.id.actionListView);
        conditionListView.removeAllViews();
        actionListView.removeAllViews();
        //os adaptadores teñen as listas de condicións e permiten engadir unha por unha as vistas para cada elemento da lista
        int adapterCount = conditionArrayAdapter.getCount();
        for (int i = 0; i < adapterCount; i++) {
            View item = conditionArrayAdapter.getView(i, null, tableGroup);
            conditionListView.addView(item);
        }
        adapterCount = actionArrayAdapter.getCount();
        for (int i = 0; i < adapterCount; i++) {
            View item = actionArrayAdapter.getView(i, null, tableGroup);
            actionListView.addView(item);
        }
    }
}