package gal.enrelar.enrelapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.ServerConnection;
import gal.enrelar.enrelapp.managers.MqttManager;
import gal.enrelar.enrelapp.managers.ServerConnectionManager;

public class ManageServerConnectionActivity extends AppCompatActivity{
    //engadir, editar, eliminar unha configuración de conexión co servidor
    private ToggleButton activeButton;
    private EditText nameContentText;
    private EditText addressContentText;
    private RadioGroup netTypeGroup;
    private EditText userContentText;
    private EditText passwordContentText;
    private EditText caCertificateContentText;
    private EditText clientCertificateContentText;
    private EditText clientKeyContentText;
    private final ServerConnectionManager serverConnectionManager = ServerConnectionManager.getInstance();
    private ServerConnection serverConnection;
    private int index;//posición da conexión na lista de conexións, pasada como parámetro

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manageserverconnection);
        //barra superior
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //engadir frecha de volver atrás
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                onBackPressed();
            }
        });

        // Iniciar variables.
        activeButton = findViewById(R.id.activeButton);
        nameContentText = findViewById(R.id.nameContent);
        addressContentText = findViewById(R.id.addressContent);
        netTypeGroup = findViewById(R.id.netTypeRadio);
        userContentText = findViewById(R.id.userContent);
        passwordContentText = findViewById(R.id.passwordContent);
        caCertificateContentText = findViewById(R.id.caCertificateContent);
        clientCertificateContentText = findViewById(R.id.clientCertificateContent);
        clientKeyContentText = findViewById(R.id.clientKeyContent);
        Intent intent = getIntent();
        String action = intent.getStringExtra("action");
        if (action == null){
            action = "no_action";
        }
        if (action.equals("edit")) {//se foi chamada dende a lista de conexións
            //toma os valores da conexión a editar
            index = intent.getIntExtra("id", 0);
            serverConnection = serverConnectionManager.getServerConnection(index);
            //cubre os campos editables cos valores da conexión
            activeButton.setChecked(serverConnection.isActive());//se é o activo pon ON
            nameContentText.setText(serverConnection.getName());
            addressContentText.setText(serverConnection.getAddress());
            switch (serverConnection.getNetType()){//seleccionar o botón que corresponde co tipo de conexión
                case "generated_wifi" : netTypeGroup.check(R.id.generated_wifiButton);
                    break;
                case "local" : netTypeGroup.check(R.id.localButton);
                    break;
                case "internet" : netTypeGroup.check(R.id.internetButton);
                    break;
            }
            userContentText.setText(serverConnection.getUser());
            passwordContentText.setText(serverConnection.getPassword());
            caCertificateContentText.setText(serverConnection.getCaCertificate());
            clientCertificateContentText.setText(serverConnection.getClientCertificate());
            clientKeyContentText.setText(serverConnection.getClientKey());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflar o menu cas iconas de eliminar e gardar
        getMenuInflater().inflate(R.menu.menu_managers, menu);
        if (serverConnection == null) {//foi chamada dende engadir
            //non é preciso botón de eliminar
            MenuItem item = menu.findItem(R.id.remove);
            item.setVisible(false);
        }
        return true;
    }

    public void saveClick(MenuItem menuItem) {
        if (menuItem.getItemId()!=R.id.save){
            return;
        }
        // foi pulsado o botón de gardar
        // campos precisos para crear ou editar a conexión
        String name;
        String address;
        String netType;
        String user;
        String password;
        String caCertificate;
        String clientCertificate;
        String clientKey;
        //boolean usado para levar control do formato dos campos
        boolean fieldsWellFormatted = true;
        //tomar o nome dende a vista
        name = nameContentText.getText().toString();
        if (name.matches("[ -]]*") || name.isEmpty()) {
            nameContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar o enderezo dende a vista
        address = addressContentText.getText().toString();
        if (address.matches("[ -]]*") || address.isEmpty() ) {
            addressContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar o tipo de rede dende a vista
        switch (netTypeGroup.getCheckedRadioButtonId()){
            case R.id.generated_wifiButton : netType = "generated_wifi";
                break;
            case R.id.localButton : netType = "local";
                break;
            case R.id.internetButton : netType = "internet";
                break;
            default : netType = "error";
                break;
        }
        //tomar o usuario dende a vista
        user = userContentText.getText().toString();
        if (user.matches("[ -]]*") || user.isEmpty()) {
            userContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar o contrasinal dende a vista
        password = passwordContentText.getText().toString();
        if (password.matches("[ -]]*") || password.isEmpty() ) {
            passwordContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar o certificado CA dende a vista
        caCertificate = caCertificateContentText.getText().toString();
        if (caCertificate.matches("[ -]]*") || caCertificate.isEmpty()) {
            caCertificateContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar o certificado de cliente dende a vista
        clientCertificate = clientCertificateContentText.getText().toString();
        if (clientCertificate.matches("[ -]]*") || clientCertificate.isEmpty()) {
            clientCertificateContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //tomar a chave de cliente dende a vista
        clientKey = clientKeyContentText.getText().toString();
        if (clientKey.matches("[ -]]*") || clientKey.isEmpty()) {
            clientKeyContentText.setError(getString(R.string.emptyFieldAd));
            fieldsWellFormatted = false;
        }
        //se tódolos campos foron  ben cubertos
        if (fieldsWellFormatted){
            if (serverConnection == null) {//se foi chamada dende engadir
                // hai que crear nova instancia
                serverConnection = new ServerConnection();
                //e obter a posición por se é marcado como activo
                index = serverConnectionManager.getServerConnectionList().size();
            }
            serverConnection.setName(name);
            serverConnection.setAddress(address);
            serverConnection.setPortNumber(netType);
            serverConnection.setUser(user);
            serverConnection.setPassword(password);
            serverConnection.setCaCertificate(caCertificate);
            serverConnection.setClientCertificate(clientCertificate);
            serverConnection.setClientKey(clientKey);
            serverConnectionManager.updateServerConnection(serverConnection);
            if (activeButton.isChecked()){//se o botón está pulsado
                //desactiva todos menos este
                serverConnectionManager.setActive(index);
            } else{//se non está pulsado marca este como inactivo
                serverConnection.setInactive();
            }
            serverConnectionManager.saveChanges();
            //rexenerar o cliente mqtt
            MqttManager mqttManager = MqttManager.getInstance();
            if (mqttManager != null){
                mqttManager.createAndConnectClient(getApplicationContext());
            }
            finish();
        }
    }
    public void removeClick(MenuItem menuItem){
        if (menuItem.getItemId()!=R.id.remove){
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.ad))
                .setMessage(getString(R.string.confirmRemove))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        serverConnectionManager.removeServerConnection(serverConnection);
                        serverConnectionManager.saveChanges();
                        finish();
                    }})
                .setNegativeButton(R.string.no, null).show();


    }

}
