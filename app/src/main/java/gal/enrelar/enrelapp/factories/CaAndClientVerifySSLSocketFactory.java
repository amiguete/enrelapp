package gal.enrelar.enrelapp.factories;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;

import android.util.Base64;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.KeyManagerFactory;

import gal.enrelar.enrelapp.Util;

public class CaAndClientVerifySSLSocketFactory extends SSLSocketFactory {
    private final SSLSocketFactory factory;

    //constructor
    public CaAndClientVerifySSLSocketFactory(String caCrt, String clientCrt, String clientKey)
            throws KeyStoreException, NoSuchAlgorithmException, IOException,
            KeyManagementException, CertificateException, UnrecoverableKeyException {
        //factoría para xerar un xestor de certificados de confianza(como a CA)
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        //almacén de chaves onde se gardará o certificado público da CA
        KeyStore caKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        caKeyStore.load(null, null);//sen certificados nin contrasinal
        //factoría para xerar certificado CA
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        //o certificado CA é un String. hai que pasalo a input stream
        InputStream inputStreamCA = Util.fromStringToInputStream(caCrt);
        //xerar o certificado dende un input stream
        X509Certificate caCertificate = (X509Certificate) certificateFactory.generateCertificate(inputStreamCA);
        //gardar o certificado no KeyStore cun alias
        caKeyStore.setCertificateEntry("CA_certificate", caCertificate);
        //gardar o KeyStore co seu certificado CA na factoría de TrustManager
        trustManagerFactory.init(caKeyStore);

        //factoría para xerar un xestor de chaves propias
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        //almacén de chaves onde se gardará o certificado e a chave de cliente
        KeyStore clientStore = KeyStore.getInstance(KeyStore.getDefaultType());
        clientStore.load(null,null);
        //xerar a chave de cliente dende string
        PrivateKey privateKey = getPrivateKey(clientKey);
        //factoría para xerar certificado cliente
        CertificateFactory clientCertificateFactory = CertificateFactory.getInstance("X.509");
        //o certificado cliente é un String. hai que pasalo a input stream
        InputStream inputStreamClient = Util.fromStringToInputStream(clientCrt);
        //xerar o certificado dende un input stream
        X509Certificate clientCertificate = (X509Certificate) clientCertificateFactory.generateCertificate(inputStreamClient);
        clientStore.setCertificateEntry("certificate", clientCertificate);
        clientStore.setKeyEntry("private-key",privateKey,new char[0], new X509Certificate[]{clientCertificate});
        keyManagerFactory.init(clientStore, new char[0]);
        //keyManagerFactory.init(null, null);//sen certificados de cliente
        //crear contexto SSL
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        //aplicar ó contexto os managers que se obteñan das respectivas factorías
        sslContext.init(keyManagerFactory.getKeyManagers(),trustManagerFactory.getTrustManagers(),null);
        //obter unha CaAndClientVerifySSLSocketFactory do sslContext para poder devolver
        factory = sslContext.getSocketFactory();
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return factory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return factory.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket() throws IOException {
        SSLSocket socket = (SSLSocket) factory.createSocket();
        socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        SSLSocket socket = (SSLSocket)factory.createSocket(s, host, port, autoClose);
        socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {

        SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
        socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        SSLSocket socket = (SSLSocket)factory.createSocket(host, port, localHost, localPort);
        socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
        socket.setEnabledProtocols(new String[]{ "TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        SSLSocket socket = (SSLSocket)factory.createSocket(address, port, localAddress,localPort);
        socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
        return socket;
    }

    private static PrivateKey getPrivateKey(String keyString) {
        //pasar de PEM String a private key
        //eliminar a cabeceira e o pé
        keyString = keyString.replace("-----BEGIN PRIVATE KEY-----", "");
        keyString = keyString.replace("-----END PRIVATE KEY-----", "");
        //pasar o String sen cabeceiras a array de bytes en base64
        byte[] bytesPKCS8 = Base64.decode(keyString,Base64.DEFAULT);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytesPKCS8);
            return keyFactory.generatePrivate(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
