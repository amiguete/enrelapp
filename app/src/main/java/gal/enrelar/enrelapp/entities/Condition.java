package gal.enrelar.enrelapp.entities;

import androidx.annotation.NonNull;

import gal.enrelar.enrelapp.Constants;

public class Condition {
    private final String deviceId;
    private final String comparator;
    private final String value;

    public Condition(String formattedString) {
        //constructor que recibe a cadea de texto formatada coma no servidor
        // separa os parámetros da condición e asígnallos ós atributos
        String[] attributes = formattedString.split(Constants.ATTRIBUTES_SPLITTER);
        this.deviceId = attributes[0];
        this.comparator = attributes[1];
        this.value = attributes[2].split("\\.")[0];//quitar décimas

    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getComparator() {
        return comparator;
    }

    public String getValue() {
        return value;
    }

    @NonNull
    @Override
    public String toString() {
        /*
         * forma e devolve un String formatado como no servidor
         */
        String formattedString;
        formattedString = deviceId + Constants.ATTRIBUTES_SEPARATOR + comparator + Constants.ATTRIBUTES_SEPARATOR + value;
        return formattedString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Action)) return false;
        Action action = (Action) o;
        return action.toString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return deviceId.hashCode() + comparator.hashCode() + value.hashCode();
    }
}
