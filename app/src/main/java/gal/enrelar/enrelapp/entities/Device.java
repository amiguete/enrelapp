package gal.enrelar.enrelapp.entities;

import androidx.annotation.NonNull;

import gal.enrelar.enrelapp.Constants;

public class Device {
    private final String deviceId;
    private final String lastValue;
    private final String deviceAlias;

    public Device(String formattedString) {
        //constructor que recibe a cadea de texto formatada coma no servidor
        // separa os parámetros do dispositivo e asígnallos ós atributos
        String[] attributes = formattedString.split(Constants.ATTRIBUTES_SPLITTER);
        this.deviceId = attributes[0];
        this.lastValue = attributes[1];
        this.deviceAlias = attributes[2];
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getLastValue() {
        return lastValue;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public String getDeviceType() {
        //devolve o tipo de dispositivo en función do id
        String type;
        type = deviceId.split("-")[0];
        return type;
    }
    @NonNull
    @Override
    public String toString() {
        /*
         * forma e devolve un String formatado como no servidor
         */
        String formattedString;
        formattedString = deviceId + Constants.ATTRIBUTES_SEPARATOR + lastValue
                + Constants.ATTRIBUTES_SEPARATOR + deviceAlias;
        return formattedString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;
        Device device = (Device) o;
        return deviceId.equals(device.getDeviceId());
    }

    @Override
    public int hashCode() {
        return deviceId.hashCode() + lastValue.hashCode() + deviceAlias.hashCode();
    }
}
