package gal.enrelar.enrelapp.entities;

import androidx.annotation.NonNull;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

import gal.enrelar.enrelapp.Constants;
import gal.enrelar.enrelapp.Util;

public class  Program {
    private int programId;
    private String name;
    private Time timeStart;
    private Time timeEnd;
    private String dayOfWeek;
    private Date dateStart;
    private Date dateEnd;
    private ArrayList<Condition> conditionList;
    private ArrayList<Action> actionList;

    public Program(String formattedString) {
        //constructor que recibe a cadea de texto formatada coma no servidor
        //separa o programa das accións e das condicións
        String[] elements = formattedString.split(Constants.ELEMENTS_SPLITTER);
        // separa os parámetros do programa e asígnallos ós atributos
        // na posición 0 de elements están os atributos do programa
        String[] attributes = elements[0].split(Constants.ATTRIBUTES_SPLITTER);
        int attributeNumber = 0;//contador de posición no array de atributos
        //cando se obtén o programa da lista de programas teñen id, ten 7 elementos
        //cando se crea un novo programa aínda non ten id polo que se pasa un atributo menos
        if (attributes.length == 7){
            //o valor -2 indica un erro na codificación
            String programIdString = attributes[attributeNumber++];
            this.programId = Integer.parseInt(programIdString);

        } else {
            //o valor -1 indica que aínda non foi enviado ó coordinador polo que non ten id
            this.programId = -1;
        }
        this.name = attributes[attributeNumber++];
        this.timeStart = Util.fromStringToTime(attributes[attributeNumber++]);//hai que engadir os segundos
        this.timeEnd = Util.fromStringToTime(attributes[attributeNumber++]);//hai que engadir os segundos
        this.dayOfWeek = attributes[attributeNumber++];
        this.dateStart = Util.fromStringToDate(attributes[attributeNumber++], false);//hai que engadir o ano
        this.dateEnd = Util.fromStringToDate(attributes[attributeNumber], false);//hai que engadir o ano
        // se existe posición 1 de elements están as accións asociadas ó programa
        // se hai varias sepáranse
        actionList = new ArrayList<>();
        if (elements.length > 1) {
            String[] actions = elements[1].split(Constants.AND_SPLITTER);
            for (String action : actions) {
                actionList.add(new Action(action));
            }
        }
        // se existe posición 2 de elements están as condicións asociadas ó programa
        // se hai varias sepáranse
        conditionList = new ArrayList<>();
        if (elements.length > 2){
            String[] conditions = elements[2].split(Constants.AND_SPLITTER);
            for (String condition : conditions) {
                conditionList.add(new Condition(condition));
            }
        }
    }

    public Program() {
        //constructor baleiro para pasarlle os atributos con setters
        actionList = new ArrayList<>();
        conditionList = new ArrayList<>();
    }

    public int getProgramId() { return programId; }

    public String getName() {
        return name;
    }

    public Time getTimeStart() {
        return timeStart;
    }

    public Time getTimeEnd() {
        return timeEnd;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public ArrayList<Condition> getConditionList() { return conditionList; }

    public ArrayList<Action> getActionList() { return actionList; }

    public void setName(String name) {
        this.name = name;
    }

    public void setTimeStart(Time timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeEnd(Time timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public void setConditionList(ArrayList<Condition> conditionList) {
        this.conditionList = conditionList;
    }

    public void setActionList(ArrayList<Action> actionList) {
        this.actionList = actionList;
    }

    @NonNull
    @Override
    public String toString() {
        /*
         * forma e devolve un String formatado como no servidor
         */
        StringBuilder formattedString;
        formattedString = new StringBuilder(name + Constants.ATTRIBUTES_SEPARATOR);
        //tomar o tempo ignorando os segundos;
        String formattedTime = Util.fromTimeToString(timeStart);
        formattedString.append(formattedTime).append(Constants.ATTRIBUTES_SEPARATOR);
        formattedTime = Util.fromTimeToString(timeEnd);
        formattedString.append(formattedTime).append(Constants.ATTRIBUTES_SEPARATOR);
        formattedString.append(dayOfWeek).append(Constants.ATTRIBUTES_SEPARATOR);
        //tomar a data ignorando o ano
        String formattedDate = Util.fromDateToString(dateStart, false);
        formattedString.append(formattedDate).append(Constants.ATTRIBUTES_SEPARATOR);
        formattedDate = Util.fromDateToString(dateEnd, false);
        formattedString.append(formattedDate).append(Constants.ELEMENTS_SEPARATOR);
        //accións
        for (int i = 0; i<actionList.size();i++) {
            //se hai mais dunha acción hai que separalas
            if (i>0){
                formattedString.append(Constants.AND_SEPARATOR);
            }
            formattedString.append(actionList.get(i).toString());
        }
        formattedString.append(Constants.ELEMENTS_SEPARATOR);
        //condicións
        for (int i = 0; i<conditionList.size();i++) {
            //se hai mais dunha condición hai que separalas
            if (i>0){
                formattedString.append(Constants.AND_SEPARATOR);
            }
            formattedString.append(conditionList.get(i).toString());
        }
        return formattedString.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Program)) return false;
        Program program = (Program) o;
        return program.toString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return programId + name.hashCode() + timeStart.hashCode() +  timeEnd.hashCode()
                + dayOfWeek.hashCode() + dateStart.hashCode() + dateEnd.hashCode()
                + conditionList.hashCode() + actionList.hashCode();
    }
}
