package gal.enrelar.enrelapp.entities;

import gal.enrelar.enrelapp.Constants;

public class ServerConnection {
    private String name;
    private String address;
    private int portNumber;
    private String user;
    private String password;
    private String caCertificate;
    private String clientCertificate;
    private String clientKey;
    private boolean active;

    public ServerConnection() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPortNumber(String netType) {
        switch (netType){
            case "generated_wifi" :   this.portNumber = Constants.GENERATED_WIFI_PORT;
                break;
            case "local" :   this.portNumber = Constants.LOCAL_PORT;
                break;
            case "internet" :   this.portNumber = Constants.INTERNET_PORT;
                break;
        }
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCaCertificate(String caCertificate) {
        this.caCertificate = caCertificate;
    }

    public void setClientCertificate(String clientCertificate) {
        this.clientCertificate = clientCertificate;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public void setActive() {
        this.active = true;
    }

    public void setInactive() {
        this.active = false;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public String getNetType(){
        switch (portNumber){
            case Constants.GENERATED_WIFI_PORT : return "generated_wifi";
            case Constants.LOCAL_PORT : return "local";
            case Constants.INTERNET_PORT : return "internet";
            default: return "error";
        }
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getCaCertificate() {
        return caCertificate;
    }

    public String getClientCertificate() {
        return clientCertificate;
    }

    public String getClientKey() {
        return clientKey;
    }

    public boolean isActive() {
        return active;
    }

}
