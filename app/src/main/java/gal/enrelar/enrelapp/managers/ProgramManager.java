package gal.enrelar.enrelapp.managers;

import java.util.ArrayList;

import gal.enrelar.enrelapp.Constants;
import gal.enrelar.enrelapp.entities.Program;

public class ProgramManager {//clase singleton
    //instancia para devolver
    private static final ProgramManager uniqueInstance = new ProgramManager();
    private static ArrayList<Program> programList;

    private ProgramManager(){
        //constructor privado
        programList = new ArrayList<>();
    }
    
    public static ProgramManager getInstance(){
        return uniqueInstance;
    }
    
    public ArrayList<Program> getProgramList(){
        return new ArrayList<>(programList);
    }
    public Program getProgram(int index) {
        return programList.get(index);
    }
    public void setProgramList(String formattedString){
        //recibe a lista de tódolos programas formatada coma no servidor
        //engade cada programa á lista de programas
        String[] programs = formattedString.split(Constants.PROGRAMS_SPLITTER);
        programList.clear();
        for (String program:programs){
            programList.add(new Program(program));
        }
    }
}
