package gal.enrelar.enrelapp.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import gal.enrelar.enrelapp.entities.ServerConnection;

public class ServerConnectionManager {
    //instancia para devolver
    private static final ServerConnectionManager uniqueInstance = new ServerConnectionManager();
    private static ArrayList<ServerConnection> serverConnectionList;
    private static SharedPreferences sharedPreferences;
    private static final String sharedPrefFile = "gal.enrelar.Enrelapp";

    private ServerConnectionManager(){
        //constructor privado
    }

    public static ServerConnectionManager getInstance(){
        return uniqueInstance;
    }

    //obter unha instancia pasándolle un contexto
    //chámase dende o método onCreate() en mainActivity para cargar a lista de servidores
    public static ServerConnectionManager getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);
        }
        if(serverConnectionList==null){
            Gson gson = new Gson();
            //cargar na lista os parámetros gardados en sharedPreferences
            String json = sharedPreferences.getString("serverConnection",null);
            serverConnectionList = gson.fromJson(json,new TypeToken<ArrayList<ServerConnection>>() {}.getType());
            //se sharedPreferences está baleiro crear unha lista nova
            if (serverConnectionList==null){
                serverConnectionList = new ArrayList<>();
            }
        }
        return uniqueInstance;
    }
    public ArrayList<ServerConnection> getServerConnectionList(){
        return new ArrayList<>(serverConnectionList);
    }

    public ServerConnection getServerConnection(int index) {
        return serverConnectionList.get(index);
    }

    public void removeServerConnection(ServerConnection serverConnection){
        serverConnectionList.remove(serverConnection);
    }

    public void updateServerConnection(ServerConnection serverConnection){
        int pos = serverConnectionList.indexOf(serverConnection);
        //se o elemento non existe engádeo á lista
        if (pos == -1){
            serverConnectionList.add(serverConnection);
        } else {//se existe substitúeo cos novos valores
            serverConnectionList.set(pos,serverConnection);
        }
    }

    public void setActive(int index){
        //marca como activo o elemento na posición index e inactivo o resto
        for (int i = 0; i < serverConnectionList.size(); i++) {
            if (i == index){
                serverConnectionList.get(i).setActive();
            } else {
                serverConnectionList.get(i).setInactive();
            }
        }
    }

    public ServerConnection getActive(){
        for (ServerConnection serverConnection:serverConnectionList){
            if(serverConnection.isActive()){
                return serverConnection;
            }
        }
        return null;//se non hai ningún activo
    }

    public void saveChanges() {
        SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(serverConnectionList);
        preferencesEditor.putString("serverConnection",json);
        preferencesEditor.apply();
    }
}
