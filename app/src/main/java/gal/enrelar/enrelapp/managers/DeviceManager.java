package gal.enrelar.enrelapp.managers;

import android.content.Context;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.Device;

public class DeviceManager {//clase singleton
    //instancia para devolver
    private static final DeviceManager uniqueInstance = new DeviceManager();
    private static ArrayList<Device> deviceList;

    //constructor privado
    private DeviceManager(){
        deviceList = new ArrayList<>();
    }

    public static DeviceManager getInstance(){
        return uniqueInstance;
    }

    public ArrayList<Device> getDeviceList(){
        return new ArrayList<>(deviceList);
    }
    public void  clearDeviceList(){
        deviceList.clear();
    }
    public void updateDevice(String formattedString){
        Device device = new Device(formattedString);
        int pos = -1;
        for (int i = 0; i < deviceList.size(); i++) {//buscar na lista se existe o dispositivo
           if (device.getDeviceId().equals(deviceList.get(i).getDeviceId())) {
               pos = i;
           }
        }
        //se o elemento non existe engádeo á lista
        if (pos == -1){
            deviceList.add(device);
        } else {//se existe substitúeo cos novos valores
            deviceList.set(pos,device);
        }
    }
    public String fromDeviceAliasToDeviceId(String deviceAlias, Context context){

        for (Device device:getDeviceList()){
            if (device.getDeviceAlias().equals(deviceAlias)){
                return device.getDeviceId();
            }
        }
        return context.getString(R.string.deviceNotConnected);
    }

    public String fromDeviceIdToDeviceAlias(String deviceId, Context context){

        for (Device device:getDeviceList()){
            if (device.getDeviceId().equals(deviceId)){
                return device.getDeviceAlias();
            }
        }
        return context.getString(R.string.deviceNotConnected);
    }

    public String[] getAllAlias(){
        String[] aliasList =new String[deviceList.size()];
        for (int i=0; i<deviceList.size(); i++) {
            aliasList[i] =deviceList.get(i).getDeviceAlias();
        }
        return aliasList;
    }
}
