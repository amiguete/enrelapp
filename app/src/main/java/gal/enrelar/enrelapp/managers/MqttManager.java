package gal.enrelar.enrelapp.managers;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


import gal.enrelar.enrelapp.Constants;
import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.adapters.TabPagerAdapter;
import gal.enrelar.enrelapp.entities.ServerConnection;
import gal.enrelar.enrelapp.factories.CaAndClientVerifySSLSocketFactory;

public class MqttManager {
    //instancia para devolver
    private static  MqttManager uniqueInstance;

    private static final ServerConnectionManager serverConnectionManager = ServerConnectionManager.getInstance();
    private MqttAndroidClient mqttAndroidClient;
    private static TabPagerAdapter adapter;
    private int toastYOffSet = 0;//variable para cambiar a posición dos toast evitando que se superpoñan
    //constructor privado
    private MqttManager(Context context){
        createAndConnectClient(context);
    }

    //Obter unha instancia pasándolle un contexto
    public static  MqttManager getInstance(Context context, TabPagerAdapter tabAdapter){
        adapter = tabAdapter;
        //se é a primeira vez que se chama hai que xerar a instancia
        if (uniqueInstance == null){
            uniqueInstance  = new MqttManager(context);
        }
        return uniqueInstance;
    }
    //Obter unha instancia
    public static  MqttManager getInstance(){
        return uniqueInstance;
    }

    public void subscribeToTopic(String topic){
        if (mqttAndroidClient != null){
            try {
                mqttAndroidClient.subscribe(topic, 0);

            } catch (MqttException ex){
                System.err.println("Exception whilst subscribing to " + topic);
                ex.printStackTrace();
            }
        }
    }

    public void publishMessage(String publishTopic, String publishMessage, Context context){
        String toastText;
        if (mqttAndroidClient != null){
            if (mqttAndroidClient.isConnected()) {
                try {
                    MqttMessage message = new MqttMessage();
                    message.setPayload(publishMessage.getBytes());
                    mqttAndroidClient.publish(publishTopic, message);
                } catch (MqttException e) {
                    System.err.println("Error Publishing: " + e.getMessage());
                    e.printStackTrace();
                }
                toastText = context.getString(R.string.sent) + " ";
                toastText += publishTopic + " " + publishMessage;
            } else{//mqttClient non está conectado
                toastText = context.getString(R.string.notSentNotConnected);
            }
        } else {//mqttClient é nulo non hai conexión activa
            toastText = context.getString(R.string.notSentNoActiveConnection);
        }
        showToast(toastText, context);
    }

    public void createAndConnectClient(final Context context){
        //se existía un cliente previo hai que pechalo
        if (mqttAndroidClient != null){
            mqttAndroidClient.close();
        }
        //xerar un identificador de cliente
        String clientId = MqttClient.generateClientId();
        //obter o servidor con conexión activa
        final ServerConnection serverConnection = serverConnectionManager.getActive();
        if (serverConnection != null) {
            //construír a URI para conexións con TLS
            //String serverUri = "tcp://" + serverConnection.getAddress() + ":" + "1883";//conexión sen seguridade
            String serverUri = "ssl://" + serverConnection.getAddress() + ":" + serverConnection.getPortNumber();
            mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId);
            mqttAndroidClient.setCallback(new MqttCallbackExtended() {

                @Override
                public void connectionLost(Throwable cause) {
                    String toastText = context.getString(R.string.connectionLost);
                    toastText += "\n";
                    toastText += context.getString(R.string.causedBy);
                    toastText += " " + cause;
                    showToast(toastText, context);
                }

                @Override
                public void messageArrived(String topic, MqttMessage message){
                    String formattedString = new String(message.getPayload());
                    switch (topic) {
                        case "getList/program":
                            ProgramManager programManager = ProgramManager.getInstance();
                            programManager.setProgramList(formattedString);

                            break;
                        case "deviceOut/status": {
                            DeviceManager deviceManager = DeviceManager.getInstance();
                            deviceManager.updateDevice(formattedString);
                            break;
                        }
                        case "deviceOut/value": {//non inclúe o alias
                            DeviceManager deviceManager = DeviceManager.getInstance();
                            String deviceId = formattedString.split(Constants.ATTRIBUTES_SPLITTER)[0];
                            formattedString += Constants.ATTRIBUTES_SEPARATOR;
                            formattedString += deviceManager.fromDeviceIdToDeviceAlias(deviceId, context);
                            deviceManager.updateDevice(formattedString);
                            break;
                        }
                        case "deviceOut/connection": {//nunha nova conexión renovar a lista
                            DeviceManager deviceManager = DeviceManager.getInstance();
                            deviceManager.clearDeviceList();
                            publishMessage("status", "", context);
                            break;
                        }
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                }

                @Override
                public void connectComplete(boolean reconnect, String serverURI) {
                    subscribeToTopic("getList/program");
                    subscribeToTopic("deviceOut/+");
                    String toastText = context.getString(R.string.connectedTo);
                    toastText += " " + serverConnection.getName();
                    toastText += " (" + serverURI + ")";
                    showToast(toastText, context);
                    publishMessage("program/getList", "", context);
                    publishMessage("status", "", context);
                }
            });
            MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setAutomaticReconnect(true);
            mqttConnectOptions.setCleanSession(true);
            mqttConnectOptions.setUserName(serverConnection.getUser());
            mqttConnectOptions.setPassword(serverConnection.getPassword().toCharArray());
            if (serverUri.startsWith("ssl://")) {
                try {
                    CaAndClientVerifySSLSocketFactory socketFactory = new CaAndClientVerifySSLSocketFactory(serverConnection.getCaCertificate(),
                            serverConnection.getClientCertificate(), serverConnection.getClientKey());
                    mqttConnectOptions.setSocketFactory(socketFactory);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {

                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                        disconnectedBufferOptions.setBufferEnabled(true);
                        disconnectedBufferOptions.setBufferSize(100);
                        disconnectedBufferOptions.setPersistBuffer(false);
                        disconnectedBufferOptions.setDeleteOldestMessages(false);
                        mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        String toastText = context.getString(R.string.connectionFailure);
                        toastText += "\n";
                        toastText += context.getString(R.string.causedBy);
                        toastText += " " + exception;
                        showToast(toastText, context);
                    }
                });


            } catch (MqttException ex) {
                ex.printStackTrace();
            }
        } else {//se non hai conexión activa
            mqttAndroidClient = null;
        }
    }
    private void showToast(String toastText, Context context){
        Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, toastYOffSet);
        if (toastYOffSet == 0){
            toastYOffSet = 150;
        } else if (toastYOffSet == 150){
            toastYOffSet = 300;
        } else if (toastYOffSet == 300){
            toastYOffSet = 0;
        }
        toast.show();
    }
}
