package gal.enrelar.enrelapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.Condition;
import gal.enrelar.enrelapp.fragments.EditConditionFragment;
import gal.enrelar.enrelapp.managers.DeviceManager;

public class ConditionArrayAdapter extends ArrayAdapter<Condition> {
    //converter un arrayList en vista
    //cada elemento ocupa unha liña co estilo definido en R.layout.list_view_condition
    private final ArrayList<Condition> conditionsList;
    private final DeviceManager deviceManager = DeviceManager.getInstance();
    //constructor
    public ConditionArrayAdapter(Context context, int resource,
                                 ArrayList<Condition> conditionsList) {
        super(context, resource, conditionsList);
        this.conditionsList=conditionsList;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        //patrón seguido para cubrir na vista cada elemento do arrayList
        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        view = inflater.inflate(R.layout.list_view_condition, parent, false);
        final Condition condition = conditionsList.get(position);
        view.setOnClickListener((new AdapterView.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                DialogFragment editConditionDialog = new EditConditionFragment(conditionsList, position);
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                editConditionDialog.show(fragmentManager,conditionsList.toString());

            }

        }));
        //cubrir os datos
        //nome de dispositivo
        TextView nameView = view.findViewById(R.id.deviceName);
        String deviceName = deviceManager.fromDeviceIdToDeviceAlias(
                condition.getDeviceId(),ConditionArrayAdapter.this.getContext());
        nameView.setText(deviceName);
        //comparador
        TextView comparatorView = view.findViewById(R.id.comparator);
        comparatorView.setText(condition.getComparator());
        //valor
        TextView valueView = view.findViewById(R.id.value);
        valueView.setText(condition.getValue());
        return view;
    }

}
