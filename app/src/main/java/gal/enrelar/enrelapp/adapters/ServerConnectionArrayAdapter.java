package gal.enrelar.enrelapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.ServerConnection;

public class ServerConnectionArrayAdapter extends ArrayAdapter<ServerConnection> {
    //converter un arrayList en vista
    //cada elemento ocupa unha liña co estilo definido en R.layout.list_view_serverConnection
    private final ArrayList<ServerConnection> serverConnectionList;
    //constructor
    public ServerConnectionArrayAdapter(Context context, int resource,
                                        ArrayList<ServerConnection> serverConnectionList) {
        super(context, resource, serverConnectionList);
        this.serverConnectionList=serverConnectionList;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        //patrón seguido para cubrir na vista cada elemento do arrayList
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        view = inflater.inflate(R.layout.list_view_serverconnection, parent, false);
        ServerConnection serverConnection = serverConnectionList.get(position);
        //cubrir os datos
        //nome
        TextView nameView = view.findViewById(R.id.name);
        nameView.setText(serverConnection.getName());
        //enderezo
        TextView addressView = view.findViewById(R.id.address);
        addressView.setText(serverConnection.getAddress());
        //tipo de conexión
        TextView netTypeView = view.findViewById(R.id.netType);
        String type = serverConnection.getNetType();
        //icona e contido
        ImageView iconView = view.findViewById(R.id.connectionIcon);
        switch (type){
            case "generated_wifi" : iconView.setImageResource(R.drawable.ic_connection_wifi);
                netTypeView.setText(R.string.generated_wifi);
                break;
            case "local" : iconView.setImageResource(R.drawable.ic_connection_local);
                netTypeView.setText(R.string.local);
                break;
            case "internet" : iconView.setImageResource(R.drawable.ic_connection_internet);
                netTypeView.setText(R.string.internet);
                break;
            default : iconView.setImageResource(R.drawable.ic_unknow);
                break;
        }
        //icona activo ou inactivo
        ImageView activeIconView = view.findViewById(R.id.activeIcon);
        if (serverConnection.isActive()){
            activeIconView.setImageResource(R.drawable.ic_server_active);
        } else{
            activeIconView.setImageResource(R.drawable.ic_server_inactive);
        }
        return view;
    }
}
