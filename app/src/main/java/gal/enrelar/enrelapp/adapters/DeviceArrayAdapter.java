package gal.enrelar.enrelapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.Device;

public class DeviceArrayAdapter extends ArrayAdapter<Device> {
    //converter un arrayList en vista
    //cada elemento ocupa unha liña co estilo definido en R.layout.list_view_device
    private final ArrayList<Device> devicesList;
    //constructor
    public DeviceArrayAdapter(Context context, int resource,
                                        ArrayList<Device> devicesList) {
        super(context, resource, devicesList);
        this.devicesList=devicesList;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        //patrón seguido para cubrir na vista cada elemento do arrayList
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        view = inflater.inflate(R.layout.list_view_device, parent, false);
        Device device = devicesList.get(position);
        //cubrir os datos
        //nome
        TextView nameView = view.findViewById(R.id.name);
        nameView.setText(device.getDeviceAlias());
        //tipo de dispositivo
        String type = device.getDeviceType();
        TextView typeView = view.findViewById(R.id.type);
        typeView.setText(type);
        //icona
        ImageView iconView = view.findViewById(R.id.deviceIcon);
        switch (type){
            case "potencia" : iconView.setImageResource(R.drawable.ic_device_power);
                break;
            case "rele" : iconView.setImageResource(R.drawable.ic_device_relay);
                break;
            case "botonera" : iconView.setImageResource(R.drawable.ic_device_buttons);
                break;
            case "sensorluz" : iconView.setImageResource(R.drawable.ic_device_light);
                break;
            case "temperatura" : iconView.setImageResource(R.drawable.ic_device_temperature);
                break;
            case "humidade" : iconView.setImageResource(R.drawable.ic_device_humidity);
                break;
            default : iconView.setImageResource(R.drawable.ic_unknow);
                break;
        }
        //valor
        TextView valueView = view.findViewById(R.id.value);
        valueView.setText(device.getLastValue());
        return view;
    }
}
