package gal.enrelar.enrelapp.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import gal.enrelar.enrelapp.fragments.ConnectionFragment;
import gal.enrelar.enrelapp.fragments.DevicesFragment;
import gal.enrelar.enrelapp.fragments.ProgramsFragment;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
    //xestiona o cambio de pestanas
    private final int numOfTabs;

    public TabPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfTabs = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position){
        final DevicesFragment tabDefault = new DevicesFragment();
        switch (position) {
            case 0: return new ConnectionFragment();
            case 1: return new DevicesFragment();
            case 2: return new ProgramsFragment();
            default: return tabDefault;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (object instanceof ConnectionFragment) {
            return POSITION_NONE;
        }
        if (object instanceof DevicesFragment) {
            return POSITION_NONE;
        }
        if (object instanceof ProgramsFragment) {
            return POSITION_NONE;
        }
        return 1;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
