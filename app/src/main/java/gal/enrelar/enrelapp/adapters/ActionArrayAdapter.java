package gal.enrelar.enrelapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.Action;
import gal.enrelar.enrelapp.fragments.EditActionFragment;
import gal.enrelar.enrelapp.managers.DeviceManager;

public class ActionArrayAdapter extends ArrayAdapter<Action> {
    //converter un arrayList en vista
    //cada elemento ocupa unha liña co estilo definido en R.layout.list_view_action
    private final ArrayList<Action> actionsList;
    private final DeviceManager deviceManager = DeviceManager.getInstance();
    //constructor
    public ActionArrayAdapter(Context context, int resource,
                              ArrayList<Action> actionsList) {
        super(context, resource, actionsList);
        this.actionsList=actionsList;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        //patrón seguido para cubrir na vista cada elemento do arrayList
        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        view = inflater.inflate(R.layout.list_view_action, parent, false);
        final Action action = actionsList.get(position);
        //poñer en cada elemento un listener que abre o editor de accións ó premer nel
        view.setOnClickListener((new AdapterView.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                DialogFragment editActionDialog = new EditActionFragment(actionsList, position);
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                editActionDialog.show(fragmentManager,actionsList.toString());

            }

        }));
        //cubrir os datos
        //nome de dispositivo
        TextView nameView = view.findViewById(R.id.deviceName);
        String deviceName = deviceManager.fromDeviceIdToDeviceAlias(
                action.getDeviceId(), ActionArrayAdapter.this.getContext());
        nameView.setText(deviceName);
        //valor
        TextView valueView = view.findViewById(R.id.value);
        valueView.setText(action.getValue());
        return view;
    }

}
