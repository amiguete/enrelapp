package gal.enrelar.enrelapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.sql.Time;
import java.util.Locale;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.Util;
import gal.enrelar.enrelapp.entities.Program;

public class ProgramArrayAdapter extends ArrayAdapter<Program> {
    //converter un arrayList en vista
    //cada elemento ocupa unha liña co estilo definido en R.layout.list_view_program
    private final ArrayList<Program> programsList;
    //constructor
    public ProgramArrayAdapter(Context context, int resource,
                              ArrayList<Program> programsList) {
        super(context, resource, programsList);
        this.programsList=programsList;
    }


    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        //patrón seguido para cubrir na vista cada elemento do arrayList
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        view = inflater.inflate(R.layout.list_view_program, parent, false);
        Program program = programsList.get(position);
        //cubrir os datos
        //nome
        TextView nameView = view.findViewById(R.id.name);
        nameView.setText(program.getName());
        //horas (tomadas como obxecto Time teñen que converterse a String ignorando os segundos)
        Time timeStart = program.getTimeStart();
        TextView timeStartView = view.findViewById(R.id.timeStart);
        timeStartView.setText(Util.fromTimeToString(timeStart));
        Time timeEnd = program.getTimeEnd();
        TextView timeEndView = view.findViewById(R.id.timeEnd);
        timeEndView.setText(Util.fromTimeToString(timeEnd));
        //datas (tomadas como obxecto Date teñen que converterse a String ignorando o ano)
        Date dateStart = program.getDateStart();
        TextView dateStartView = view.findViewById(R.id.dateStart);
        dateStartView.setText(Util.fromDateToString(dateStart, true));
        Date dateEnd = program.getDateEnd();
        TextView dateEndView = view.findViewById(R.id.dateEnd);
        dateEndView.setText(Util.fromDateToString(dateEnd, true));
        //días da semana
        String dayOfWeek = program.getDayOfWeek();
        TextView dayOfWeekView = view.findViewById(R.id.dow);
        dayOfWeekView.setText(Util.fromNumberToDayOfWeek(dayOfWeek,getContext()));
        //icona varía en función de se está activo o programa ou non
        ImageView iconView = view.findViewById(R.id.programIcon);
        Calendar calendar = Calendar.getInstance();
        String currentDayOfWeek = String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1);//-1 para que vaia de 0 a 6
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        Date currentDate = Date.valueOf(dateFormatter.format(calendar.getTime()));
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss",Locale.ENGLISH);
        Time currentTime = Time.valueOf(timeFormatter.format(calendar.getTime()));
        boolean isActive = true;
        //día da semana inclúe hoxe?
        if (!dayOfWeek.contains(currentDayOfWeek)){
            isActive = false;
        } else if (currentDate.after(dateEnd) || currentDate.before(dateStart)){
            isActive = false;
        } else if (currentTime.after(timeEnd) || currentTime.before(timeStart)){
            isActive = false;
        }
        if (isActive){
            iconView.setImageResource(R.drawable.ic_program_active);
        } else {
            iconView.setImageResource(R.drawable.ic_program_inactive);
        }
        return view;
    }
}
