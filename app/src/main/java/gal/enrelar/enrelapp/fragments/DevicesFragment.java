package gal.enrelar.enrelapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.adapters.DeviceArrayAdapter;
import gal.enrelar.enrelapp.entities.Device;
import gal.enrelar.enrelapp.managers.DeviceManager;

public class DevicesFragment extends Fragment {
    private final DeviceManager deviceManager = DeviceManager.getInstance();

    //constructor baleiro
    public DevicesFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_devices, container, false);
        final Context context = container.getContext();
        final ArrayList<Device> devicesList = deviceManager.getDeviceList();
        if (devicesList.isEmpty()){
            //amosar que a lista está baleira e é preciso actualizar
            TextView advertiseText;
            advertiseText= view.findViewById(R.id.emptyListAdviseText);
            advertiseText.setText(getString(R.string.emptyDeviceListAdvise));
        } else {
            DeviceArrayAdapter adapter =
                    new DeviceArrayAdapter(context, R.layout.list_view_device, devicesList);
            ListView deviceListView = view.findViewById(R.id.ListView);
            deviceListView.setAdapter(adapter);
            deviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Device device = devicesList.get((int)id);
                    DialogFragment sendValueDialog = new SendValueFragment(device);
                    FragmentManager fragmentManager = getFragmentManager();
                    assert fragmentManager != null;
                    sendValueDialog.show(fragmentManager,device.getDeviceId());
                }
            });
        }
        return view;
    }
}
