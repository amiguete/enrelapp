package gal.enrelar.enrelapp.fragments;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.entities.Device;
import gal.enrelar.enrelapp.managers.MqttManager;

public class SendValueFragment extends DialogFragment {
    //aparece cando se preme sobre un dispositivo da lista de dispositivos
    //amosa información do dispositivo e no caso dos relés e do xestores de potencia permite enviar un valor
    private Device device;
    private RadioGroup onOffRadioGroup;
    private SeekBar powerSeekBar;
    //constructor
    public SendValueFragment() {
        this.device = null;
    }
    public SendValueFragment(Device device) {
        this.device = device;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String formattedString = savedInstanceState.getString("device");
            if (formattedString != null){
                device = new Device(formattedString);
            }
        }
        // Usar a clase Builder para obter un constructor de AlertDialog
        Activity activity = getActivity();
        if (activity == null){
            activity = new Activity();
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        // Definir as características do diálogo usando o constructor
        //título
        builder.setTitle(device.getDeviceAlias());
        //vista
        // tomar o LayoutInflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        // seleccionar e inflar o layout, obtense unha vista
        View view = inflater.inflate(R.layout.fragment_send_value, null);
        //encher a vista cos datos
        TextView nameView = view.findViewById(R.id.name);
        nameView.setText(device.getDeviceAlias());
        TextView valueView = view.findViewById(R.id.value);
        valueView.setText(device.getLastValue());
        powerSeekBar = view.findViewById(R.id.seekBar);//a barra deslizante
        final TextView valueOnSeekBar = view.findViewById(R.id.valueOnSeekBar);//o valor que amosa a barra deslizante
        onOffRadioGroup =  view.findViewById(R.id.onOffRadio);
        //facer visible o campo de valor se é relé ou se é potencia
        if (device.getDeviceType().equals("potencia")){
            powerSeekBar.setVisibility(View.VISIBLE);
            valueOnSeekBar.setVisibility(View.VISIBLE);
            if (savedInstanceState != null) {//se se reconstrúe toma os valores anteriores
                int savedValue = savedInstanceState.getInt("seekBarValue");
                valueOnSeekBar.setText(String.valueOf(savedValue));
                powerSeekBar.setProgress(savedValue);
            } else{
                valueOnSeekBar.setText(device.getLastValue());
                powerSeekBar.setProgress(Integer.parseInt(device.getLastValue()));
            }
            powerSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                                        @Override
                                                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                                            valueOnSeekBar.setText(String.valueOf(progress));
                                                        }

                                                        @Override
                                                        public void onStartTrackingTouch(SeekBar seekBar) {

                                                        }

                                                        @Override
                                                        public void onStopTrackingTouch(SeekBar seekBar) {

                                                        }
                                                    });
        } else if (device.getDeviceType().equals("rele")){
            onOffRadioGroup.setVisibility(View.VISIBLE);
            if (savedInstanceState != null) {//se se reconstrúe toma os valores anteriores
                int savedButtonChecked = savedInstanceState.getInt("radioValue");
                onOffRadioGroup.check(savedButtonChecked);
            } else {
                //poñer o valor actual no botón
                if (device.getLastValue().equals("1")){
                    onOffRadioGroup.check(R.id.onRadio);
                } else{
                    onOffRadioGroup.check(R.id.offRadio);
                }
            }
        } else {//calquera outro tipo de dispositivo distinto de relé e de potencia
            //cambiar a mensaxe para informar que non se pode enviar nada
            TextView messageView = view.findViewById(R.id.message);
            messageView.setText(R.string.nothingToSendAd);
            messageView.setTextSize(15);
        }

        setButtons(device.getDeviceType(), builder);
        //definir view como vista no constructor
        builder.setView(view);
        // Crear o obxecto AlertDialog a partir do constructor e devólveo
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // gardar o dispositivo e a elección do valor
        savedInstanceState.putString("device", device.toString());
        savedInstanceState.putInt("radioValue",onOffRadioGroup.getCheckedRadioButtonId());
        savedInstanceState.putInt("seekBarValue",powerSeekBar.getProgress());
    }

    private void setButtons(String deviceType, AlertDialog.Builder builder ){
        //definir a acción dos botóns en función do tipo de dispositivo
        switch (deviceType){
            case "rele" :
                builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    String topic = device.getDeviceId() + "/action";
                    String message;
                    //ler valor a enviar
                    if(onOffRadioGroup.getCheckedRadioButtonId()==R.id.onRadio){
                        message = "1";
                    } else if (onOffRadioGroup.getCheckedRadioButtonId()==R.id.offRadio){
                        message = "0";
                    } else{
                        message = "error";
                    }
                    MqttManager mqttManager = MqttManager.getInstance();
                    mqttManager.publishMessage(topic, message, SendValueFragment.this.getContext());
                }
            });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                break;
            case "potencia" :
                builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String topic = device.getDeviceId() + "/action";
                        int valueToSend = powerSeekBar.getProgress();
                        String message = String.valueOf(valueToSend);
                        MqttManager mqttManager = MqttManager.getInstance();
                        mqttManager.publishMessage(topic, message, SendValueFragment.this.getContext());
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
                break;
            default : builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
                break;
        }
    }
}
