package gal.enrelar.enrelapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.activities.ManageServerConnectionActivity;
import gal.enrelar.enrelapp.adapters.ServerConnectionArrayAdapter;
import gal.enrelar.enrelapp.entities.ServerConnection;
import gal.enrelar.enrelapp.managers.ServerConnectionManager;

public class ConnectionFragment extends Fragment {
    private final ServerConnectionManager serverConnectionManager = ServerConnectionManager.getInstance();

    //constructor baleiro
    public ConnectionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection, container, false);
        final Context context = container.getContext();
        ArrayList<ServerConnection> serverConnectionList = serverConnectionManager.getServerConnectionList();
        if (serverConnectionList.isEmpty()){
            //amosar que a lista está baleira e é preciso crear unha conexión
            TextView advertiseText;
            advertiseText= view.findViewById(R.id.emptyListAdviseText);
            advertiseText.setText(getString(R.string.emptyConnectionListAdvise));
        } else {
            ServerConnectionArrayAdapter adapter =
                    new ServerConnectionArrayAdapter(context, R.layout.list_view_serverconnection, serverConnectionList);
            ListView serverConnectionListView = view.findViewById(R.id.ListView);
            serverConnectionListView.setAdapter(adapter);
            serverConnectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Intent intent = new Intent(context, ManageServerConnectionActivity.class);
                    intent.putExtra("id",(int)id);
                    intent.putExtra("action","edit");
                    startActivity(intent);
                }
            });
        }
        return view;
    }
}
