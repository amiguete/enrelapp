package gal.enrelar.enrelapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.activities.ManageProgramActivity;
import gal.enrelar.enrelapp.adapters.ProgramArrayAdapter;
import gal.enrelar.enrelapp.entities.Program;
import gal.enrelar.enrelapp.managers.ProgramManager;

public class ProgramsFragment extends Fragment {
    private final ProgramManager programManager = ProgramManager.getInstance();
    //constructor baleiro
    public ProgramsFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_programs, container, false);
        final Context context = container.getContext();
        final ArrayList<Program> programsList = programManager.getProgramList();
        if (programsList.isEmpty()){
            //amosar que a lista está baleira e é preciso actualizar
            TextView advertiseText;
            advertiseText= view.findViewById(R.id.emptyListAdviseText);
            advertiseText.setText(getString(R.string.emptyProgramListAdvise));
        } else {
            ProgramArrayAdapter adapter =
                    new ProgramArrayAdapter(context, R.layout.list_view_program, programsList);
            ListView programListView = view.findViewById(R.id.ListView);
            programListView.setAdapter(adapter);
            programListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Intent intent = new Intent(context, ManageProgramActivity.class);
                    intent.putExtra("id",(int)id);
                    intent.putExtra("action","edit");
                    startActivity(intent);
                }
            });
        }
        return view;
    }
}
