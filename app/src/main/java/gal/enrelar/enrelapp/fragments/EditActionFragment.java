package gal.enrelar.enrelapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

import gal.enrelar.enrelapp.Constants;
import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.activities.ManageProgramActivity;
import gal.enrelar.enrelapp.entities.Action;
import gal.enrelar.enrelapp.managers.DeviceManager;

public class EditActionFragment extends DialogFragment {
    private  int position;
    private  ArrayList<Action> actionsList;
    private  ManageProgramActivity manageProgramActivity;
    private  final DeviceManager deviceManager = DeviceManager.getInstance();
    private AutoCompleteTextView deviceView;
    private View view = null;
    //empty constructor
    public EditActionFragment() {//constructor chamado cando se recrea o fragmento
        //provisional ata capturar os gardados o rotar pantalla
        this.actionsList = null;
        this.position = -1;
    }
    //constructor recibe a lista de accións, e a posición da lista na que está a acción a editar
    public EditActionFragment(ArrayList<Action> actionsList, int position) {
        this.actionsList = actionsList;
        this.position = position;
    }
    @SuppressLint("InflateParams")
    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //se foi chamado con constructor baleiro debe haber unha savedInstanceState
        if (savedInstanceState != null) {
            actionsList = (ArrayList<Action>) savedInstanceState.getSerializable("actionsList");
            position = savedInstanceState.getInt("position");
        }
        manageProgramActivity = (ManageProgramActivity) getActivity();
        // Usar a clase Builder para obter un constructor de AlertDialog
        Activity activity = getActivity();
        assert activity != null;
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        // Definir as características do diálogo usando o constructor
        //título
        builder.setTitle(R.string.actions);
        //vista
        // tomar o LayoutInflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        // seleccionar e inflar o layout, obtense unha vista
        view = inflater.inflate(R.layout.fragment_edit_condition_action, null);
        //eliminar tódalas cousas que son precisas en editar condición pero aquí non
        ViewGroup comparatorLayout = view.findViewById(R.id.comparatorLayout);
        comparatorLayout.setVisibility(View.INVISIBLE);
        RadioButton gButton = view.findViewById(R.id.gRadio);
        gButton.setText("");
        if (savedInstanceState != null){
            String deviceId = savedInstanceState.getString("deviceId");
            assert deviceId != null;
            defineElementsToShow(deviceId);
        }
        //poñer a icona de eliminar co seu listener
        manageRemoveIcon(view);
        //lista de tódolos nomes de dispositivo
        String[] allDevicesAliasList = deviceManager.getAllAlias();
        //ArrayList de nomes de dispositivo que poden recibir accións
        ArrayList<String> devicesAliasArrayList = new ArrayList<>();
        for (String alias : allDevicesAliasList) {
            String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                    alias, EditActionFragment.this.getContext());
            if (deviceId.contains("potencia-") || deviceId.contains("rele-")) {
                devicesAliasArrayList.add(alias);
            }
        }
        //Array de String de nomes de dispositivo que poden recibir accións
        String[] devicesAliasList = new String[devicesAliasArrayList.size()];
        devicesAliasArrayList.toArray(devicesAliasList);
        if (devicesAliasList.length == 0){
            devicesAliasList = new String[1];
            devicesAliasList[0] = getString(R.string.emptyDeviceListAdvise);
        }
        //adaptador para xerar a vista da lista
        Context context = getContext();
        assert context != null;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(

                context, R.layout.pop_up_device_alias, devicesAliasList);
        //tomar valores das vistas
        //menú despregable de nomes de dispositivo
        deviceView = view.findViewById(R.id.deviceMenu);
        deviceView.setAdapter(adapter);
        deviceView.setInputType(InputType.TYPE_NULL);
        ImageView dropDownIcon = view.findViewById(R.id.dropdown);
        //listener para cando se pulsa no nome amose a lista
        dropDownIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceView.setText("");
                deviceView.showDropDown();
            }
        });
        //cargar vistas das posibilidades de valor
        final RadioGroup onOffView = view.findViewById(R.id.onOffRGBRadio);
        final RadioButton onButton = view.findViewById(R.id.onRRadio);
        final RadioButton offButton = view.findViewById(R.id.offBRadio);
        final TextView valueOnSeekBar = view.findViewById(R.id.valueOnSeekBar);
        final SeekBar seekBar = view.findViewById(R.id.seekBar);
        deviceView.setOnItemClickListener(new AdapterView.OnItemClickListener () {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                        deviceView.getText().toString(), EditActionFragment.this.getContext());
                //en función do tipo de dispositivo seleccionado amosa unhas posibilidades ou outras
                defineElementsToShow(deviceId);
            }

        });
        //listener que elimina a advertencia de campo baleiro creada manualmente
        View.OnClickListener radioListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.adIcon).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.adText).setVisibility(View.INVISIBLE);
            }
        };
        onOffView.setOnClickListener(radioListener);
        onButton.setOnClickListener(radioListener);
        offButton.setOnClickListener(radioListener);
        if (position != -1 && savedInstanceState == null) {//se foi chamado dende a lista de condicións e non foi recargado
            //encher a vista cos datos
            Action action = actionsList.get(position);
            String deviceId = action.getDeviceId();
            String deviceName = deviceManager.fromDeviceIdToDeviceAlias(deviceId, getContext());
            //en función do tipo de dispositivo seleccionado amosa unhas posibilidades ou outras
            defineElementsToShow(deviceId);
            deviceView.setText(deviceName, false);
            String value = action.getValue();
            if (deviceId.contains("potencia-")){
                valueOnSeekBar.setText(value);
                seekBar.setProgress(Integer.parseInt(action.getValue()));
            } else if (deviceId.contains("rele-")){
                if (value.equals("0")) {
                    onOffView.check(R.id.offBRadio);
                } else if (value.equals("1")){
                    onOffView.check(R.id.onRRadio);
                }
            }
        }
        builder.setPositiveButton(R.string.save, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        //definir view como vista no constructor
        builder.setView(view);
        // Crear o obxecto AlertDialog a partir do constructor
        AlertDialog dialog = builder.create();
        // define o comportamento do botón positivo
        //está definido a posteriori de crear o diálogo para evitar que cando se pulse OK
        //con campos baleiros desapareza
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String device = deviceView.getText().toString();
                        String value;
                        String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                                deviceView.getText().toString(), EditActionFragment.this.getContext());
                        if (deviceId.contains("rele-")) {
                            //comprobar valores de radioButton
                            if(onOffView.getCheckedRadioButtonId()==R.id.onRRadio){
                                value = "1";
                            } else if (onOffView.getCheckedRadioButtonId()==R.id.offBRadio){
                                value = "0";
                            } else{
                                value = "";
                            }
                        } else if (deviceId.contains("potencia-")) {
                            //tomar valor da barra
                            value = (String) valueOnSeekBar.getText();

                        } else {
                            value = "";
                        }
                        //evitar campos baleiros
                        if (device.isEmpty()) {
                            deviceView.setError(getText(R.string.emptyFieldAd));
                            return;
                        }
                        if (value.isEmpty()) {
                            view.findViewById(R.id.adIcon).setVisibility(View.VISIBLE);
                            view.findViewById(R.id.adText).setVisibility(View.VISIBLE);
                            return;
                        }
                        String formattedString =
                                deviceManager.fromDeviceAliasToDeviceId(device, getContext());
                        formattedString += Constants.ATTRIBUTES_SEPARATOR;
                        formattedString += value;
                        Action action = new Action(formattedString);
                        if (position != -1) {//se foi chamado dende a lista de condicións
                            actionsList.set(position, action);//modifica a acción
                        } else {//se foi chamado dende engadir
                            actionsList.add(action);//engade unha nova
                        }
                        manageProgramActivity.recreateListView();
                        EditActionFragment.this.dismiss();
                    }
                });
            }
        });
        return dialog;
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //como o reconstruír o fragmento en rotacións chama ó constructor baleiro
        //é preciso gardar estes valores
        // gardar as listas
        savedInstanceState.putSerializable("actionsList", actionsList);
        // gardar a posición
        savedInstanceState.putInt("position", position);
        //gardar o identificador do dispositivo para recargar os elementos axeitados
        String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                deviceView.getText().toString(), EditActionFragment.this.getContext());
        savedInstanceState.putString("deviceId", deviceId);
        //garda os valores das vistas co método da super clase
        super.onSaveInstanceState(savedInstanceState);
    }


    private void manageRemoveIcon(View view) {
        ImageView removeIcon = view.findViewById(R.id.remove);
        if(position==-1){//se é unha nova condición
            //non se amosa a icona
            removeIcon.setVisibility(View.INVISIBLE);
        }
        removeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//se foi pulsada a icona de eliminar
                actionsList.remove(position);//eliminar a condición da lista
                manageProgramActivity.recreateListView();//recargar a lista
                EditActionFragment.this.dismiss();
            }
        });
    }

    private void defineElementsToShow(String deviceId){
        //cargar vistas das posibilidades de valor
        final RadioGroup onOffView = view.findViewById(R.id.onOffRGBRadio);
        final RadioButton onButton = view.findViewById(R.id.onRRadio);
        final RadioButton offButton = view.findViewById(R.id.offBRadio);
        final ViewGroup seekGroup = view.findViewById(R.id.seekGroup);
        final SeekBar seekBar = view.findViewById(R.id.seekBar);
        final TextView valueOnSeekBar = view.findViewById(R.id.valueOnSeekBar);
        final TextView units = view.findViewById(R.id.units);
        if (deviceId.contains("rele-")) {
            //en Valor amosar opción onOff
            seekGroup.setVisibility(View.INVISIBLE);
            onOffView.setVisibility(View.VISIBLE);
            onButton.setText(getText(R.string.on));
            offButton.setText(getText(R.string.off));
        } else if (deviceId.contains("potencia-")) {
            //en Valor amosar opción seekBar 0-100
            seekGroup.setVisibility(View.VISIBLE);
            onOffView.setVisibility(View.INVISIBLE);
            seekBar.setMax(100);
            seekBar.setProgress(50);
            valueOnSeekBar.setText(String.valueOf(seekBar.getProgress()));
            units.setText(R.string.percent);
            //listener para barra de valores
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    valueOnSeekBar.setText(String.valueOf(progress));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }
}
