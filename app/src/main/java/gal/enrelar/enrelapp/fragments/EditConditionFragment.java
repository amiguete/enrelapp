package gal.enrelar.enrelapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

import gal.enrelar.enrelapp.Constants;
import gal.enrelar.enrelapp.R;
import gal.enrelar.enrelapp.activities.ManageProgramActivity;
import gal.enrelar.enrelapp.entities.Condition;
import gal.enrelar.enrelapp.managers.DeviceManager;

public class EditConditionFragment extends DialogFragment {
    private  int position;
    private  ArrayList<Condition> conditionsList;
    private  ManageProgramActivity manageProgramActivity;
    private  final DeviceManager deviceManager = DeviceManager.getInstance();
    private AutoCompleteTextView deviceView;
    private View view = null;
    //empty constructor
    public EditConditionFragment() {//constructor chamado cando se recrea o fragmento
        //provisional ata capturar os gardados o rotar pantalla
        this.conditionsList = null;
        this.position = -1;
    }
    //constructor recibe a lista de condicións, e a posición da lista na que está a condición a editar
    public EditConditionFragment(ArrayList<Condition> conditionsList, int position) {
        this.conditionsList = conditionsList;
        this.position = position;
    }
    @SuppressLint("InflateParams")
    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //se foi chamado con constructor baleiro debe haber unha savedInstanceState
        if (savedInstanceState != null) {
            conditionsList = (ArrayList<Condition>) savedInstanceState.getSerializable("conditionsList");
            position = savedInstanceState.getInt("position");
        }
        manageProgramActivity = (ManageProgramActivity) getActivity();
        // Usar a clase Builder para obter un constructor de AlertDialog
        Activity activity = getActivity();
        assert activity != null;
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        // Definir as características do diálogo usando o constructor
        //título
        builder.setTitle(R.string.condition);
        //vista
        // tomar o LayoutInflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        // seleccionar e inflar o layout, obtense unha vista
        view = inflater.inflate(R.layout.fragment_edit_condition_action, null);
        if (savedInstanceState != null){
            String deviceId = savedInstanceState.getString("deviceId");
            assert deviceId != null;
            defineElementsToShow(deviceId);
        }
        //poñer a icona de eliminar co seu listener
        manageRemoveIcon(view);
        //lista de nomes de dispositivo
        String[] devicesAliasList = deviceManager.getAllAlias();
        if (devicesAliasList.length == 0){
            devicesAliasList = new String[1];
            devicesAliasList[0] = getString(R.string.emptyDeviceListAdvise);
        }
        //adaptador para xerar a vista da lista
        Context context = getContext();
        assert context != null;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context, R.layout.pop_up_device_alias, devicesAliasList);
        //tomar valores das vistas
        //menú despregable de nomes de dispositivo
        deviceView = view.findViewById(R.id.deviceMenu);
        deviceView.setAdapter(adapter);
        deviceView.setInputType(InputType.TYPE_NULL);
        ImageView dropDownIcon = view.findViewById(R.id.dropdown);
        //listener para cando se pulsa no nome amose a lista
        dropDownIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceView.setText("");
                deviceView.showDropDown();
            }
        });
        //cargar vistas dos botóns do comparador
        final RadioGroup comparatorView = view.findViewById(R.id.comparatorRadio);
        final RadioButton equalButton = view.findViewById(R.id.equalRadio);
        //cargar vistas das posibilidades de valor
        final RadioGroup onOffRGBView = view.findViewById(R.id.onOffRGBRadio);
        final RadioButton onRButton = view.findViewById(R.id.onRRadio);
        final RadioButton gButton = view.findViewById(R.id.gRadio);
        final RadioButton offBButton = view.findViewById(R.id.offBRadio);
        final TextView valueOnSeekBar = view.findViewById(R.id.valueOnSeekBar);
        final SeekBar seekBar = view.findViewById(R.id.seekBar);

        deviceView.setOnItemClickListener(new AdapterView.OnItemClickListener () {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                        deviceView.getText().toString(), EditConditionFragment.this.getContext());
                //en función do tipo de dispositivo seleccionado amosa unhas posibilidades ou outras
                defineElementsToShow(deviceId);
            }

        });
        //listener que elimina a advertencia de campo baleiro creada manualmente
        View.OnClickListener radioListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.adIcon).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.adText).setVisibility(View.INVISIBLE);
            }
        };
        onOffRGBView.setOnClickListener(radioListener);
        onRButton.setOnClickListener(radioListener);
        offBButton.setOnClickListener(radioListener);
        gButton.setOnClickListener(radioListener);
        if (position != -1 && savedInstanceState == null) {//se foi chamado dende a lista de condicións e non foi recargado
            //encher a vista cos datos
            Condition condition = conditionsList.get(position);
            String deviceId = condition.getDeviceId();
            String deviceName = deviceManager.fromDeviceIdToDeviceAlias(deviceId, getContext());
            //en función do tipo de dispositivo seleccionado amosa unhas posibilidades ou outras
            defineElementsToShow(deviceId);
            deviceView.setText(deviceName, false);
            if (deviceId.contains("rele-")) {//se é relé hai que validar 0 ou 1
                switch (condition.getValue()) {
                    case "1":
                        onOffRGBView.check(R.id.onRRadio);
                        break;
                    case "0":
                        onOffRGBView.check(R.id.offBRadio);
                        break;
                }
            } else if (deviceId.contains("botonera-")){//se é botonera hai que validar a cor
                switch (condition.getValue()) {
                    case "R":
                        onOffRGBView.check(R.id.onRRadio);
                        break;
                    case "G":
                        onOffRGBView.check(R.id.gRadio);
                        break;
                    case "B":
                        onOffRGBView.check(R.id.offBRadio);
                        break;
                }
            } else {
                switch (condition.getComparator()){//tódolos demais teñen o comparador con 3 opcións e a barra de desprazamento
                    case "menor":
                        comparatorView.check(R.id.lessRadio);
                        break;
                    case "igual":
                        comparatorView.check(R.id.equalRadio);
                        break;
                    case "maior":
                        comparatorView.check(R.id.greatRadio);
                        break;
                }
                valueOnSeekBar.setText(condition.getValue());
                seekBar.setProgress(Integer.parseInt(condition.getValue()));
            }
        }
        builder.setPositiveButton(R.string.save, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        //definir view como vista no constructor
        builder.setView(view);
        // Crear o obxecto AlertDialog a partir do constructor
        AlertDialog dialog = builder.create();
        // define o comportamento do botón positivo
        //está definido a posteriori de crear o diálogo para evitar que cando se pulse OK
        //con campos baleiros desapareza
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String device = deviceView.getText().toString();
                        String comparator;
                        switch (comparatorView.getCheckedRadioButtonId()){
                            case R.id.lessRadio:
                                comparator = "menor";
                                break;
                            case R.id.equalRadio:
                                comparator = "igual";
                                break;
                            case R.id.greatRadio:
                                comparator = "maior";
                                break;
                            default:
                                comparator = "";
                                break;
                        }
                        String value;
                        String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                                deviceView.getText().toString(), EditConditionFragment.this.getContext());
                        if (deviceId.contains("botonera-")) {
                            //comprobar valor de radioButton
                            switch (onOffRGBView.getCheckedRadioButtonId()){
                                case R.id.onRRadio:
                                    value = "R";
                                    break;
                                case R.id.gRadio:
                                    value = "G";
                                    break;
                                case R.id.offBRadio:
                                    value = "B";
                                    break;
                                default:
                                    value = "";
                                    break;
                            }

                        } else if (deviceId.contains("rele-")) {
                            //comprobar valores de radioButton
                            if(onOffRGBView.getCheckedRadioButtonId()==R.id.onRRadio){
                                value = "1";
                            } else if (onOffRGBView.getCheckedRadioButtonId()==R.id.offBRadio){
                                value = "0";
                            } else{
                                value = "";
                            }
                        } else if (deviceId.contains("potencia-") ||
                                deviceId.contains("sensorluz-") ||
                                deviceId.contains("temperatura-") ||
                                deviceId.contains("humidade-")) {
                            //tomar valor da barra
                            value = (String) valueOnSeekBar.getText();

                        } else {
                            value = "";
                        }
                        //evitar campos baleiros
                        if (device.isEmpty()) {
                            deviceView.setError(getText(R.string.emptyFieldAd));
                            return;
                        }
                        if (comparator.isEmpty()) {
                            equalButton.setError(getText(R.string.emptyFieldAd));
                            return;
                        }
                        if (value.isEmpty()) {
                            view.findViewById(R.id.adIcon).setVisibility(View.VISIBLE);
                            view.findViewById(R.id.adText).setVisibility(View.VISIBLE);
                            return;
                        }
                        String formattedString =
                                deviceManager.fromDeviceAliasToDeviceId(device, getContext());
                        formattedString += Constants.ATTRIBUTES_SEPARATOR;
                        formattedString += comparator;
                        formattedString += Constants.ATTRIBUTES_SEPARATOR;
                        formattedString += value;
                        Condition condition = new Condition(formattedString);
                        if (position != -1) {//se foi chamado dende a lista de condicións
                            conditionsList.set(position, condition);
                        } else {
                            conditionsList.add(condition);
                        }
                        manageProgramActivity.recreateListView();
                        EditConditionFragment.this.dismiss();
                    }
                });
            }
        });
        return dialog;
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //como o reconstruír o fragmento en rotacións chama ó constructor baleiro
        //é preciso gardar estes valores
        // gardar as listas
        savedInstanceState.putSerializable("conditionsList", conditionsList);
        // gardar a posición
        savedInstanceState.putInt("position", position);
        //gardar o identificador do dispositivo para recargar os elementos axeitados
        String deviceId = deviceManager.fromDeviceAliasToDeviceId(
                deviceView.getText().toString(), EditConditionFragment.this.getContext());
        savedInstanceState.putString("deviceId", deviceId);
        //garda os valores das vistas co método da super clase
        super.onSaveInstanceState(savedInstanceState);
    }


    private void manageRemoveIcon(View view) {
        ImageView removeIcon = view.findViewById(R.id.remove);
        if(position==-1){//se é unha nova condición
            //non se amosa a icona
            removeIcon.setVisibility(View.INVISIBLE);
        }
        removeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//se foi pulsada a icona de eliminar
                conditionsList.remove(position);//eliminar a condición da lista
                manageProgramActivity.recreateListView();//recargar a lista
                EditConditionFragment.this.dismiss();
            }
        });
    }

    private void defineElementsToShow(String deviceId){
        //cargar vistas dos botóns do comparador
        final RadioGroup comparatorView = view.findViewById(R.id.comparatorRadio);
        final RadioButton lessButton = view.findViewById(R.id.lessRadio);
        final RadioButton greatButton = view.findViewById(R.id.greatRadio);
        //cargar vistas das posibilidades de valor
        final RadioGroup onOffRGBView = view.findViewById(R.id.onOffRGBRadio);
        final RadioButton onRButton = view.findViewById(R.id.onRRadio);
        final RadioButton gButton = view.findViewById(R.id.gRadio);
        final RadioButton offBButton = view.findViewById(R.id.offBRadio);
        final ViewGroup seekGroup = view.findViewById(R.id.seekGroup);
        final SeekBar seekBar = view.findViewById(R.id.seekBar);
        final TextView valueOnSeekBar = view.findViewById(R.id.valueOnSeekBar);
        final TextView units = view.findViewById(R.id.units);
        boolean setSeekBarListener = false;
        if (deviceId.contains("botonera-")) {
            //deixar so o botón de igual no comparador
            lessButton.setVisibility(View.INVISIBLE);
            greatButton.setVisibility(View.INVISIBLE);
            comparatorView.check(R.id.equalRadio);
            //en Valor amosar opción RGB
            seekGroup.setVisibility(View.INVISIBLE);
            onOffRGBView.setVisibility(View.VISIBLE);
            onRButton.setText(getText(R.string.red));
            gButton.setVisibility(View.VISIBLE);
            gButton.setText(R.string.green);
            offBButton.setText(getText(R.string.blue));

        } else if (deviceId.contains("rele-")) {
            lessButton.setVisibility(View.INVISIBLE);
            greatButton.setVisibility(View.INVISIBLE);
            comparatorView.check(R.id.equalRadio);
            //en Valor amosar opción onOff
            seekGroup.setVisibility(View.INVISIBLE);
            onOffRGBView.setVisibility(View.VISIBLE);
            onRButton.setText(getText(R.string.on));
            gButton.setVisibility(View.INVISIBLE);
            gButton.setText("");
            offBButton.setText(getText(R.string.off));
        } else if (deviceId.contains("potencia-")) {
            lessButton.setVisibility(View.VISIBLE);
            greatButton.setVisibility(View.VISIBLE);
            //en Valor amosar opción seekBar 0-100
            seekGroup.setVisibility(View.VISIBLE);
            onOffRGBView.setVisibility(View.INVISIBLE);
            seekBar.setMax(100);
            seekBar.setProgress(50);
            valueOnSeekBar.setText(String.valueOf(seekBar.getProgress()));
            units.setText(R.string.percent);
            setSeekBarListener = true;

        } else if (deviceId.contains("sensorluz-")) {
            lessButton.setVisibility(View.VISIBLE);
            greatButton.setVisibility(View.VISIBLE);
            //en Valor amosar opción seekBar 0-1000
            seekGroup.setVisibility(View.VISIBLE);
            onOffRGBView.setVisibility(View.INVISIBLE);
            seekBar.setMax(1000);
            seekBar.setProgress(500);
            valueOnSeekBar.setText(String.valueOf(seekBar.getProgress()));
            units.setText(R.string.permil);
            setSeekBarListener = true;

        } else if (deviceId.contains("temperatura-")) {
            lessButton.setVisibility(View.VISIBLE);
            greatButton.setVisibility(View.VISIBLE);
            //en Valor amosar opción seekBar 0-1000
            seekGroup.setVisibility(View.VISIBLE);
            onOffRGBView.setVisibility(View.INVISIBLE);
            seekBar.setMax(50);
            seekBar.setProgress(20);
            valueOnSeekBar.setText(String.valueOf(seekBar.getProgress()));
            units.setText(R.string.celsius);
            setSeekBarListener = true;

        } else if (deviceId.contains("humidade-")) {
            lessButton.setVisibility(View.VISIBLE);
            greatButton.setVisibility(View.VISIBLE);
            //en Valor amosar opción seekBar 0-1000
            seekGroup.setVisibility(View.VISIBLE);
            onOffRGBView.setVisibility(View.INVISIBLE);
            seekBar.setMax(90);
            seekBar.setProgress(60);
            valueOnSeekBar.setText(String.valueOf(seekBar.getProgress()));
            units.setText(R.string.percent);
            setSeekBarListener = true;

        }
        //listener para barra de valores
        if (setSeekBarListener){
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    valueOnSeekBar.setText(String.valueOf(progress));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }
}
